# -*- coding: utf-8 -*-

"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

from qgis.PyQt.QtCore import QCoreApplication, QVariant
from qgis.PyQt.QtGui import QIcon

from qgis.core import (
    QgsProcessing,
    QgsFeatureRequest,
    QgsProcessingAlgorithm,
    QgsProcessingParameterRasterLayer,
    QgsProcessingParameterNumber,
    QgsProcessingParameterDistance,
    QgsProcessingParameterField,
    QgsProcessingParameterFeatureSource,
    QgsProcessingParameterDefinition,
    QgsProcessingParameterFolderDestination,
    QgsProcessingParameterString,
    QgsFeatureRequest,
    QgsRasterLayer,
    QgsVectorLayer,
    QgsRaster,
    QgsFields,
    QgsField,
    QgsVectorFileWriter,
    QgsProcessingUtils,
    QgsWkbTypes,
    NULL
    )
from qgis import processing

import os

from .utils import get_raster_stats

pluginPath = os.path.dirname(__file__)


class WetAlgorithm(QgsProcessingAlgorithm):
    """
    WET - Wadi Evalutation Processing Script
    """
    WADI_SUBBASIN = 'WADI_SUBBASIN'

    FLOW_ACCUMULATION_01 = 'FLOW_ACCUMULATION_01'
    FLOW_ACCUMULATION_WEIGHT_01 = 'FLOW_ACCUMULATION_WEIGHT_01'
    SUBBASIN_SLOPE_02 = 'SUBBASIN_SLOPE_02'
    SUBBASIN_SLOPE_WEIGHT_02 = 'SUBBASIN_SLOPE_WEIGHT_02'
    MAXIMUM_FLOW_03 = 'MAXIMUM_FLOW_03'
    MAXIMUM_FLOW_WEIGHT_03 = 'MAXIMUM_FLOW_WEIGHT_03'

    PRECIPITATION_HEIGHT_04 = 'PRECIPITATION_HEIGHT_04'
    PRECIPITATION_HEIGHT_WEIGHT_04 = 'PRECIPITATION_HEIGHT_WEIGHT_04'
    PRECIPITATION_INTENSITY_05 = 'PRECIPITATION_INTENSITY_05'
    PRECIPITATION_INTENSITY_WEIGHT_05 = 'PRECIPITATION_INTENSITY_WEIGHT_05'
    DRY_PERIOD_06 = 'DRY_PERIOD_06'
    DRY_PERIOD_WEIGHT_06 = 'DRY_PERIOD_WEIGHT_06'
    TEMPERATURE_07 = 'TEMPERATURE_07'
    TEMPERATURE_WEIGHT_07 = 'TEMPERATURE_WEIGHT_07'
    INTERCEPTION_08 = 'INTERCEPTION_08'
    INTERCEPTION_WEIGHT_08 = 'INTERCEPTION_WEIGHT_08'
    RETENTION_09 = 'RETENTION_09'
    RETENTION_WEIGHT_09 = 'RETENTION_WEIGHT_09'

    SAND_CONTENT_10 = 'SAND_CONTENT_10'
    SAND_CONTENT_WEIGHT_10 = 'SAND_CONTENT_WEIGHT_10'
    STONE_CONTENT_11 = 'STONE_CONTENT_11'
    STONE_CONTENT_WEIGHT_11 = 'STONE_CONTENT_WEIGHT_11'
    SUBSURFACE_PERMEABILITY_12 = 'SUBSURFACE_PERMEABILITY_12'
    SUBSURFACE_PERMEABILITY_WEIGHT_12 = 'SUBSURFACE_PERMEABILITY_WEIGHT_12'
    INFILTRATION_13 = 'INFILTRATION_13'
    INFILTRATION_WEIGHT_13 = 'INFILTRATION_WEIGHT_13'
    SOIL_SALINITY_14 = 'SOIL_SALINITY_14'
    SOIL_SALINITY_WEIGHT_14 = 'SOIL_SALINITY_WEIGHT_14'


    STREAM_LOCATION_15 = 'STREAM_LOCATION_15'
    RIVER_BANKS_HEIGHT_16 = 'RIVER_BANKS_HEIGHT_16'
    RIVER_BANKS_HEIGHT_MIN_16 = 'RIVER_BANKS_HEIGHT_MIN_16'
    RIVER_BANKS_WIDTH_17 = 'RIVER_BANKS_WIDTH_17'
    RIVER_BANKS_WIDTH_MIN_17 = 'RIVER_BANKS_WIDTH_MIN_17'
    SLOPE_18 = 'SLOPE_18'
    SLOPE_MIN = 'SLOPE_MIN'
    SLOPE_MAX = 'SLOPE_MAX'
    SUITABLE_PERMEABILITY_19 = 'SUITABLE_PERMEABILITY_19'
    SHALLOW_GROUNDWATER_EC_20 = 'SHALLOW_GROUNDWATER_EC_20'
    SHALLOW_GROUNDWATER_EC_FIELD_20 = 'SHALLOW_GROUNDWATER_EC_FIELD_20'
    SHALLOW_GROUNDWATER_EC_THRESHOLD_20 = 'SHALLOW_GROUNDWATER_EC_THRESHOLD_20'
    SHALLOW_GROUNDWATER_EC_DISTANCE_20 = 'SHALLOW_GROUNDWATER_EC_DISTANCE_20'

    LOCATION_WATER_SITE_21 = 'LOCATION_WATER_SITE_21'
    LOCATION_WATER_SITE_WEIGHT_21 = 'LOCATION_WATER_SITE_WEIGHT_21'
    LOCATION_WATER_SITE_DISTANCE_21 = 'LOCATION_WATER_SITE_DISTANCE_21'
    LOCATION_ROADS_22 = 'LOCATION_ROADS_22'
    LOCATION_ROADS_WEIGHT_22 = 'LOCATION_ROADS_WEIGHT_22'
    LOCATION_ROADS_DISTANCE_22 = 'LOCATION_ROADS_DISTANCE_22'
    LOCATION_ROUTES_23 = 'LOCATION_ROUTES_23'
    LOCATION_ROUTES_WEIGHT_23 = 'LOCATION_ROUTES_WEIGHT_23'
    LOCATION_ROUTES_DISTANCE_23 = 'LOCATION_ROUTES_DISTANCE_23'
    MARKETS_24 = 'MARKETS_24'
    MARKETS_WEIGHT_24 = 'MARKETS_WEIGHT_24'
    MARKETS_DISTANCE_24 = 'MARKETS_DISTANCE_24'
    DISTRICTS_CAPITALS_25 = 'DISTRICTS_CAPITALS_25'
    DISTRICTS_CAPITALS_WEIGHT_25 = 'DISTRICTS_CAPITALS_WEIGHT_25'
    DISTRICTS_CAPITALS_DISTANCE_25 = 'DISTRICTS_CAPITALS_DISTANCE_25'
    SETTLEMENTS_26 = 'SETTLEMENTS_26'
    SETTLEMENTS_WEIGHT_26 = 'SETTLEMENTS_WEIGHT_26'
    SETTLEMENTS_DISTANCE_26 = 'SETTLEMENTS_DISTANCE_26'
    SAND_CONTENT_27 = 'SAND_CONTENT_27'
    SAND_CONTENT_WEIGHT_27 = 'SAND_CONTENT_WEIGHT_27'
    STONE_CONTENT_28 = 'STONE_CONTENT_28'
    STONE_CONTENT_WEIGHT_28 = 'STONE_CONTENT_WEIGHT_28'
    DRY_PERIOD_29 = 'DRY_PERIOD_29'
    DRY_PERIOD_WEIGHT_29 = 'DRY_PERIOD_WEIGHT_29'
    OWNED_LAND_30 = 'OWNED_LAND_30'
    OWNED_LAND_WEIGHT_30 = 'OWNED_LAND_WEIGHT_30'


    SIMULATION_ID = 'SIMULATION_ID'
    # SITE_REQUIREMENTS_OUTPUT = 'SITE_REQUIREMENTS_OUTPUT'
    OUTPUT_FOLDER = 'OUTPUT_FOLDER'

    # additional parameter for raster cell size
    RASTER_CELL = 'RASTER_CELL'

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return WetAlgorithm()

    def name(self):
        return 'wet'

    def displayName(self):
        return self.tr('WET - Wadi Evaluation')

    def group(self):
        return self.tr('WET - Wadi Evaluation')

    def groupId(self):
        return 'wet-wadi'

    def icon(self):
        return QIcon(os.path.join(pluginPath, "icon.png"))

    def shortHelpString(self):
        return self.tr("WET - Wadi Evaluation Tool Script")

    def helpUrl(self):
        return "https://gitlab.com/faunalia/wadi_evaluation_tool/-/blob/master/WET-WadiEvaluationTool_manual.pdf"

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # (a) Wadi Subbasin (Required)

        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.WADI_SUBBASIN,
                self.tr('Wadi Subbasin (required'),
                [QgsProcessing.TypeVectorPolygon]
            )
        )

        # (b) Geometry Parameter Layers
        
        # 01 Flow Accumulation
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.FLOW_ACCUMULATION_01,
                self.tr('01 Flow Accumulation'),
                optional=True
            )
        )
        # 01 Flow Accumulation Weight
        self.addParameter(
            QgsProcessingParameterNumber(
                self.FLOW_ACCUMULATION_WEIGHT_01,
                self.tr('01 - Flow Accumulation Weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=3,
                optional=True
            )
        )

        # 02 Subbasin Slope
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.SUBBASIN_SLOPE_02,
                self.tr('02 Subbasin Slope'),
                optional=True
            )
        )
        # 02 Subbasin Slope Weight
        self.addParameter(
            QgsProcessingParameterNumber(
                self.SUBBASIN_SLOPE_WEIGHT_02,
                self.tr('02 - Subbasin Slope Weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=1,
                optional=True
            )
        )

        # 03 Maximum Flow Lenght
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.MAXIMUM_FLOW_03,
                self.tr('03 Maximum Flow Lenght'),
                optional=True
            )
        )
        # 03 Maximum Flow Lenght Weight
        self.addParameter(
            QgsProcessingParameterNumber(
                self.MAXIMUM_FLOW_WEIGHT_03,
                self.tr('03 - Maximum Flow Lenght Weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=-2,
                optional=True
            )
        )


        # (c) Climate and Hydrology Parameter Grids

        # 04 precipitation height
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.PRECIPITATION_HEIGHT_04,
                self.tr('04 Precipitation Height'),
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.PRECIPITATION_HEIGHT_WEIGHT_04,
                self.tr('04 - Precipitation Height Weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=5,
                optional=True
            )
        )

        # 05 precipitation intensity (optional)
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.PRECIPITATION_INTENSITY_05,
                self.tr('05 Precipitation Intensity'),
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.PRECIPITATION_INTENSITY_WEIGHT_05,
                self.tr('05 - Precipitation Intensity Weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=2,
                optional=True
            )
        )

        # 06 dry period vegetation
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.DRY_PERIOD_06,
                self.tr('06 Dry Period Vegetation'),
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.DRY_PERIOD_WEIGHT_06,
                self.tr('06 - Dry Period Vegetation Weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=8,
                optional=True
            )
        )

        # 07 temperature
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.TEMPERATURE_07,
                self.tr('07 Temperature'),
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.TEMPERATURE_WEIGHT_07,
                self.tr('07 - Temperature Weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=-2,
                optional=True
            )
        )

        # 08 Interception
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INTERCEPTION_08,
                self.tr('08 Interception'),
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.INTERCEPTION_WEIGHT_08,
                self.tr('08 - Interception Weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=-1,
                optional=True
            )
        )

        # 09 Retention
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.RETENTION_09,
                self.tr('09 Retention'),
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.RETENTION_WEIGHT_09,
                self.tr('09 - Retention Weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=-2,
                optional=True
            )
        )


        # (d) Soil Parameter Layers

        # 10 sand content
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.SAND_CONTENT_10,
                self.tr('10 Sand Content'),
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.SAND_CONTENT_WEIGHT_10,
                self.tr('10 - Sand Content Weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=1,
                optional=True
            )
        )

        # 11 stone content
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.STONE_CONTENT_11,
                self.tr('11 Stone Content'),
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.STONE_CONTENT_WEIGHT_11,
                self.tr('11 - Stone Content Weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=1,
                optional=True
            )
        )

        # 12 subsurface permeability
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.SUBSURFACE_PERMEABILITY_12,
                self.tr('12 Subsurface Permeability'),
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.SUBSURFACE_PERMEABILITY_WEIGHT_12,
                self.tr('12 - Subsurface Permeability Weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=-6,
                optional=True
            )
        )

        # 13 infiltration
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INFILTRATION_13,
                self.tr('13 Infiltration'),
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.INFILTRATION_WEIGHT_13,
                self.tr('13 - Infiltration Weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=-3,
                optional=True
            )
        )

        # 14 soil salinity
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.SOIL_SALINITY_14,
                self.tr('14 Soil Salinity'),
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.SOIL_SALINITY_WEIGHT_14,
                self.tr('14 - Soil Salinity Weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=-3,
                optional=True
            )
        )


        # (e) Site Requirement Layers

        # 15 Stream Location (required layer)
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.STREAM_LOCATION_15,
                self.tr('15 Location of Streams (required)'),
            )
        )

        # 16 River Banks Height (optional)
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.RIVER_BANKS_HEIGHT_16,
                self.tr('16 River Banks Height'),
                optional=True
            )
        )
        # 16 River Banks Height Minimum
        self.addParameter(
            QgsProcessingParameterNumber(
                self.RIVER_BANKS_HEIGHT_MIN_16,
                self.tr("17 - River Banks Minimum Height Threshold (m)"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=3,
                optional=True
            )
        )

        # 17 River Banks Width (optional)
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.RIVER_BANKS_WIDTH_17,
                self.tr('17 River Banks Width'),
                optional=True
            )
        )
        # 17 River Banks Width Minimum
        self.addParameter(
            QgsProcessingParameterNumber(
                self.RIVER_BANKS_WIDTH_MIN_17,
                self.tr("17 - River Banks Minimum Width Threshold (m)"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=25,
                optional=True
            )
        )

        # 18 Slope
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.SLOPE_18,
                self.tr('18 Slope'),
                optional=True
            )
        )
        # 18 Minimum Slope
        self.addParameter(
            QgsProcessingParameterNumber(
                self.SLOPE_MIN,
                self.tr("18 - Minimum Slope (%)"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=0.01,
                minValue=0,
                maxValue=4,
                optional=True
            )
        )
        # 18 Maximum Slope
        self.addParameter(
            QgsProcessingParameterNumber(
                self.SLOPE_MAX,
                self.tr('18 - Maximum Slope (%)'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=4,
                minValue=0.01,
                maxValue=4,
                optional=True
            )
        )

        # 19 Suitable permeability
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.SUITABLE_PERMEABILITY_19,
                self.tr('19 Suitable Subsurface Permeability'),
                optional=True
            )
        )

        # 20 Shallow Groundwater EC
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.SHALLOW_GROUNDWATER_EC_20,
                self.tr('20 Shallow Groundwater Conductivity'),
                [QgsProcessing.TypeVectorPoint],
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterField(
                self.SHALLOW_GROUNDWATER_EC_FIELD_20,
                self.tr('20 - Shallow Groundwater Conductivity Field'),
                defaultValue='EC_MYSCM',
                parentLayerParameterName=self.SHALLOW_GROUNDWATER_EC_20,
                type=QgsProcessingParameterField.Numeric,
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.SHALLOW_GROUNDWATER_EC_THRESHOLD_20,
                self.tr('20 - Shallow Groundwater Conductivity Threshold (uSm)'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=2000,
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterDistance(
                self.SHALLOW_GROUNDWATER_EC_DISTANCE_20,
                self.tr('20 - Shallow Groundwater Conductivity Distance Buffer'),
                defaultValue=1500,
                parentParameterName=self.SHALLOW_GROUNDWATER_EC_20,
                minValue=1,
                optional=True

            )
        )


        # (f) Site Suitability Parameters

        # 21 Location Water Site
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.LOCATION_WATER_SITE_21,
                self.tr('21 Location of Water Sites'),
                [QgsProcessing.TypeVectorPoint],
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.LOCATION_WATER_SITE_WEIGHT_21,
                self.tr('21 - Location of Water Sites Weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=5,
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterDistance(
                self.LOCATION_WATER_SITE_DISTANCE_21,
                self.tr('21 - Location of Water Sites Distance Buffer'),
                defaultValue=1500,
                parentParameterName=self.LOCATION_WATER_SITE_21,
                minValue=1,
                optional=True
            )
        )

        # 22 Location of roads
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.LOCATION_ROADS_22,
                self.tr('22 Location of Roads'),
                [QgsProcessing.TypeVectorLine],
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.LOCATION_ROADS_WEIGHT_22,
                self.tr('22 - Location of Roads Weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=2,
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterDistance(
                self.LOCATION_ROADS_DISTANCE_22,
                self.tr('22 - Location of Roads Distance Buffer'),
                defaultValue=1000,
                parentParameterName=self.LOCATION_ROADS_22,
                minValue=1,
                optional=True
            )
        )

        # 23 Location of Cattle Routes
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.LOCATION_ROUTES_23,
                self.tr('23 Location of Cattle Routes'),
                [QgsProcessing.TypeVectorLine],
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.LOCATION_ROUTES_WEIGHT_23,
                self.tr('23 - Location of Cattle Routes Weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=4,
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterDistance(
                self.LOCATION_ROUTES_DISTANCE_23,
                self.tr('23 - Location of Cattle Routes Distance Buffer'),
                defaultValue=2000,
                parentParameterName=self.LOCATION_ROUTES_23,
                minValue=1,
                optional=True
            )
        )

        # 24 Location of lifestock markets
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.MARKETS_24,
                self.tr('24 Location of Lifestock Markets'),
                [QgsProcessing.TypeVectorPoint],
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.MARKETS_WEIGHT_24,
                self.tr('24 - Location of Lifestock Markets Weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=1,
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterDistance(
                self.MARKETS_DISTANCE_24,
                self.tr('24 - Location of Lifestock Markets Distance Buffer'),
                defaultValue=20000,
                parentParameterName=self.MARKETS_24,
                minValue=1,
                optional=True
            )
        )

        # 25 Location of districts capitals 
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.DISTRICTS_CAPITALS_25,
                self.tr('25 Location of District Capitals'),
                [QgsProcessing.TypeVectorPoint],
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.DISTRICTS_CAPITALS_WEIGHT_25,
                self.tr('25 - Location of District Capitals Weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=3,
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterDistance(
                self.DISTRICTS_CAPITALS_DISTANCE_25,
                self.tr('25 - Location of District Capitals Distance Buffer'),
                defaultValue=20000,
                parentParameterName=self.DISTRICTS_CAPITALS_25,
                minValue=1,
                optional=True
            )
        )

        # 26 Location of other settlements 
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.SETTLEMENTS_26,
                self.tr('26 Location of Other Settlements'),
                [QgsProcessing.TypeVectorPoint],
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.SETTLEMENTS_WEIGHT_26,
                self.tr('26 - Location of Other Settlements Weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=2,
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterDistance(
                self.SETTLEMENTS_DISTANCE_26,
                self.tr('26 - Location of Other Settlements Distance Buffer'),
                defaultValue=5000,
                parentParameterName=self.SETTLEMENTS_26,
                minValue=1,
                optional=True
            )
        )

        # 27 Sand Contents
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.SAND_CONTENT_27,
                self.tr('27 Sand Contents'),
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.SAND_CONTENT_WEIGHT_27,
                self.tr("27 - Sand Contents Weight"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=1,
                optional=True
            )
        )

        # 28 Stone Contents
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.STONE_CONTENT_28,
                self.tr('28 Stone Contents'),
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.STONE_CONTENT_WEIGHT_28,
                self.tr("28 - Stone Contents Weight"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=1,
                optional=True
            )
        )

        # 29 Dry Period Vegetation
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.DRY_PERIOD_29,
                self.tr('29 Dry Period Vegetation'),
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.DRY_PERIOD_WEIGHT_29,
                self.tr("29 - Dry Period Vegetation Weight"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=5,
                optional=True
            )
        )

        # 30 Pubblically owned land
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.OWNED_LAND_30,
                self.tr('30 Public Owned Land'),
                optional=True
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.OWNED_LAND_WEIGHT_30,
                self.tr("30 - Public Owned Land Weight"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=1,
                optional=True
            )
        )



        # raster cell size
        raster_cell_parameter = QgsProcessingParameterNumber(
            self.RASTER_CELL,
            self.tr('Raster Cell Size'),
            type=QgsProcessingParameterNumber.Integer,
            defaultValue=100,
            minValue=1
        )
        raster_cell_parameter.setFlags(raster_cell_parameter.flags() | QgsProcessingParameterDefinition.FlagAdvanced)
        self.addParameter(raster_cell_parameter)


        # Site Requirements Output
        # self.addParameter(
        #     QgsProcessingParameterRasterDestination(
        #         self.SITE_REQUIREMENTS_OUTPUT,
        #         self.tr('Site Requirements Output')
        #     )
        # )

        # simulation ID
        self.addParameter(
            QgsProcessingParameterString(
                self.SIMULATION_ID,
                self.tr('Simulation ID'),
                defaultValue='01'
            )
        )

        # output folder where to save all the layers
        self.addParameter(
            QgsProcessingParameterFolderDestination(
                self.OUTPUT_FOLDER,
                self.tr('Output folder'),
                defaultValue=QgsProcessingUtils.tempFolder()
            )
        )


    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """

        # define all the inputs from the UI

        # (a)
        wadi_subbasin = self.parameterAsSource(
            parameters,
            self.WADI_SUBBASIN,
            context
        )


        # (b)
        flow_accumulation_01 = self.parameterAsRasterLayer(
            parameters,
            self.FLOW_ACCUMULATION_01,
            context
        )

        flow_accumulation_weight_01 = self.parameterAsDouble(
            parameters,
            self.FLOW_ACCUMULATION_WEIGHT_01,
            context
        )

        subbasin_slope_02 = self.parameterAsRasterLayer(
            parameters,
            self.SUBBASIN_SLOPE_02,
            context
        )

        subbasin_slope_weight_02 = self.parameterAsDouble(
            parameters,
            self.SUBBASIN_SLOPE_WEIGHT_02,
            context
        )

        max_flow_lenght_03 = self.parameterAsRasterLayer(
            parameters,
            self.MAXIMUM_FLOW_03,
            context
        )

        max_flow_lenght_weight_03 = self.parameterAsDouble(
            parameters,
            self.MAXIMUM_FLOW_WEIGHT_03,
            context
        )


        # (c)
        precipitation_height_04 = self.parameterAsRasterLayer(
            parameters,
            self.PRECIPITATION_HEIGHT_04,
            context
        )
        precipitation_height_weight_04 = self.parameterAsDouble(
            parameters,
            self.PRECIPITATION_HEIGHT_WEIGHT_04,
            context
        )

        precipitation_intensity_05 = self.parameterAsRasterLayer(
            parameters,
            self.PRECIPITATION_INTENSITY_05,
            context
        )
        precipitation_intensity_weight_05 = self.parameterAsDouble(
            parameters,
            self.PRECIPITATION_INTENSITY_WEIGHT_05,
            context
        )

        dry_period_06 = self.parameterAsRasterLayer(
            parameters,
            self.DRY_PERIOD_06,
            context
        )
        dry_period_weight_06 = self.parameterAsDouble(
            parameters,
            self.DRY_PERIOD_WEIGHT_06,
            context
        )

        temperature_07 = self.parameterAsRasterLayer(
            parameters,
            self.TEMPERATURE_07,
            context
        )
        temperature_weight_07 = self.parameterAsDouble(
            parameters,
            self.TEMPERATURE_WEIGHT_07,
            context
        )

        interception_08 = self.parameterAsRasterLayer(
            parameters,
            self.INTERCEPTION_08,
            context
        )
        interception_weight_08 = self.parameterAsDouble(
            parameters,
            self.INTERCEPTION_WEIGHT_08,
            context
        )

        retention_09 = self.parameterAsRasterLayer(
            parameters,
            self.RETENTION_09,
            context
        )
        retention_weight_09 = self.parameterAsDouble(
            parameters,
            self.RETENTION_WEIGHT_09,
            context
        )


        # (d)

        sand_content_10 = self.parameterAsRasterLayer(
            parameters,
            self.SAND_CONTENT_10,
            context
        )
        sand_content_weight_10 = self.parameterAsDouble(
            parameters,
            self.SAND_CONTENT_WEIGHT_10,
            context
        )

        stone_content_11 = self.parameterAsRasterLayer(
            parameters,
            self.STONE_CONTENT_11,
            context
        )
        stone_content_weight_11 = self.parameterAsDouble(
            parameters,
            self.STONE_CONTENT_WEIGHT_11,
            context
        )

        subsurface_permeability_12 = self.parameterAsRasterLayer(
            parameters,
            self.SUBSURFACE_PERMEABILITY_12,
            context
        )
        subsurface_permeability_weight_12 = self.parameterAsDouble(
            parameters,
            self.SUBSURFACE_PERMEABILITY_WEIGHT_12,
            context
        )

        infiltration_13 = self.parameterAsRasterLayer(
            parameters,
            self.INFILTRATION_13,
            context
        )
        infiltration_weight_13 = self.parameterAsDouble(
            parameters,
            self.INFILTRATION_WEIGHT_13,
            context
        )

        soil_salinity_14 = self.parameterAsRasterLayer(
            parameters,
            self.SOIL_SALINITY_14,
            context
        )
        soil_salinity_weight_14 = self.parameterAsDouble(
            parameters,
            self.SOIL_SALINITY_WEIGHT_14,
            context
        )


        # (e)
        stream_location_15 = self.parameterAsRasterLayer(
            parameters,
            self.STREAM_LOCATION_15,
            context
        )

        river_banks_height_16 = self.parameterAsRasterLayer(
            parameters,
            self.RIVER_BANKS_HEIGHT_16,
            context
        )
        river_banks_heigth_min_16 = self.parameterAsDouble(
            parameters,
            self.RIVER_BANKS_HEIGHT_MIN_16,
            context
        )

        river_banks_width_17 = self.parameterAsRasterLayer(
            parameters,
            self.RIVER_BANKS_WIDTH_17,
            context
        )
        river_banks_width_min_17 = self.parameterAsDouble(
            parameters,
            self.RIVER_BANKS_WIDTH_MIN_17,
            context
        )

        slope_18 = self.parameterAsRasterLayer(
            parameters,
            self.SLOPE_18,
            context
        )
        slope_18_min = self.parameterAsDouble(
            parameters,
            self.SLOPE_MIN,
            context
        )
        slope_18_max = self.parameterAsDouble(
            parameters,
            self.SLOPE_MAX,
            context
        )

        suitable_permeability_19 = self.parameterAsRasterLayer(
            parameters,
            self.SUITABLE_PERMEABILITY_19,
            context
        )

        shallow_groundwater_ec_20 = self.parameterAsSource(
            parameters,
            self.SHALLOW_GROUNDWATER_EC_20,
            context
        )
        shallow_groundwater_ec_field_20 = self.parameterAsFields(
            parameters,
            self.SHALLOW_GROUNDWATER_EC_FIELD_20,
            context
        )[0]
        shallow_groundwater_ec_threshold_20 = self.parameterAsDouble(
            parameters,
            self.SHALLOW_GROUNDWATER_EC_THRESHOLD_20,
            context
        )
        shallow_groundwater_ec_distance_20 = self.parameterAsDouble(
            parameters,
            self.SHALLOW_GROUNDWATER_EC_DISTANCE_20,
            context
        )

        location_water_site_21 = self.parameterAsSource(
            parameters,
            self.LOCATION_WATER_SITE_21,
            context
        )
        location_water_site_weight_21 = self.parameterAsDouble(
            parameters,
            self.LOCATION_WATER_SITE_WEIGHT_21,
            context
        )
        location_water_site_distance_21 = self.parameterAsDouble(
            parameters,
            self.LOCATION_WATER_SITE_DISTANCE_21,
            context
        )

        location_roads_22 = self.parameterAsSource(
            parameters,
            self.LOCATION_ROADS_22,
            context
        )
        location_roads_weight_22 = self.parameterAsDouble(
            parameters,
            self.LOCATION_ROADS_WEIGHT_22,
            context
        )
        location_roads_distance_22 = self.parameterAsDouble(
            parameters,
            self.LOCATION_ROADS_DISTANCE_22,
            context
        )

        location_routes_23 = self.parameterAsSource(
            parameters,
            self.LOCATION_ROUTES_23,
            context
        )
        location_routes_weight_23 = self.parameterAsDouble(
            parameters,
            self.LOCATION_ROUTES_WEIGHT_23,
            context
        )
        location_routes_distance_23 = self.parameterAsDouble(
            parameters,
            self.LOCATION_ROUTES_DISTANCE_23,
            context
        )

        location_markets_24 = self.parameterAsSource(
            parameters,
            self.MARKETS_24,
            context
        )
        location_markets_weight_24 = self.parameterAsDouble(
            parameters,
            self.MARKETS_WEIGHT_24,
            context
        )
        location_markets_distance_24 = self.parameterAsDouble(
            parameters,
            self.MARKETS_DISTANCE_24,
            context
        )

        location_districts_25 = self.parameterAsSource(
            parameters,
            self.DISTRICTS_CAPITALS_25,
            context
        )
        location_districts_weight_25 = self.parameterAsDouble(
            parameters,
            self.DISTRICTS_CAPITALS_WEIGHT_25,
            context
        )
        location_districts_distance_25 = self.parameterAsDouble(
            parameters,
            self.DISTRICTS_CAPITALS_DISTANCE_25,
            context
        )

        settlements_26 = self.parameterAsSource(
            parameters,
            self.SETTLEMENTS_26,
            context
        )
        settlements_weight_26 = self.parameterAsDouble(
            parameters,
            self.SETTLEMENTS_WEIGHT_26,
            context
        )
        settlements_distance_26 = self.parameterAsDouble(
            parameters,
            self.SETTLEMENTS_DISTANCE_26,
            context
        )

        sand_content_27 = self.parameterAsRasterLayer(
            parameters,
            self.SAND_CONTENT_27,
            context
        )
        sand_content_weight_27 = self.parameterAsDouble(
            parameters,
            self.SAND_CONTENT_WEIGHT_27,
            context
        )

        stone_content_28 = self.parameterAsRasterLayer(
            parameters,
            self.STONE_CONTENT_28,
            context
        )
        stone_content_weight_28 = self.parameterAsDouble(
            parameters,
            self.STONE_CONTENT_WEIGHT_28,
            context
        )

        dry_period_29 = self.parameterAsRasterLayer(
            parameters,
            self.DRY_PERIOD_29,
            context
        )
        dry_period_weight_29 = self.parameterAsDouble(
            parameters,
            self.DRY_PERIOD_WEIGHT_29,
            context
        )

        owned_land_30 = self.parameterAsRasterLayer(
            parameters,
            self.OWNED_LAND_30,
            context
        )
        owned_land_weight_30 = self.parameterAsDouble(
            parameters,
            self.OWNED_LAND_WEIGHT_30,
            context
        )


        # additional raster cell parameters
        raster_cell = self.parameterAsInt(
            parameters,
            self.RASTER_CELL,
            context
        )

        # Output(s)
        # site_requirements_output_raster = self.parameterAsOutputLayer(
        #     parameters,
        #     self.SITE_REQUIREMENTS_OUTPUT,
        #     context
        # )


        # simulation id
        simulation_id = self.parameterAsString(
            parameters,
            self.SIMULATION_ID,
            context
        )

        # Output folder
        output_folder = self.parameterAsString(
            parameters,
            self.OUTPUT_FOLDER,
            context
        )

        # create a copy of the input wadi subbasin layer to work on
        # wadi_subbasin_copy = wadi_subbasin.materialize(QgsFeatureRequest())
        wadi_subbasin_geom = QgsWkbTypes.displayString(wadi_subbasin.wkbType())
        wadi_subasin_crs = wadi_subbasin.sourceCrs().authid()
        wadi_subbasin_copy = QgsVectorLayer(
            "{}?crs={}".format(
                wadi_subbasin_geom, 
                wadi_subasin_crs
            ),
            'name',
            'memory'
        )

        wadi_subasin_fields = wadi_subbasin.fields().toList()
        feedback.pushDebugInfo(str(wadi_subasin_fields))
        wadi_subbasin_features = [feat for feat in wadi_subbasin.getFeatures()]

        wadi_subbasin_copy_pr = wadi_subbasin_copy.dataProvider()
        wadi_subbasin_copy_pr.addAttributes(wadi_subasin_fields)
        wadi_subbasin_copy.updateFields()
        wadi_subbasin_copy_pr.addFeatures(wadi_subbasin_features)

        # FIRST TAB - SUBBASIN PARAMETERS

        # create subdir(s) if not exists
        # make parent directory with the simulation name
        simulation_folder = 'simulation_{}'.format(simulation_id)
        if not os.path.exists(os.path.join(output_folder, simulation_folder)):
            os.makedirs(os.path.join(output_folder, simulation_folder))

        if not os.path.exists(os.path.join(output_folder, simulation_folder, 'SubbasinDataForZonalStatistics')):
            os.makedirs(os.path.join(
                output_folder,
                simulation_folder,
                'SubbasinDataForZonalStatistics'
                )
            )
        if not os.path.exists(os.path.join(output_folder, simulation_folder, 'SiteSuitabilityDataForAdding')):
            os.makedirs(os.path.join(
                output_folder, 
                simulation_folder,
                'SiteSuitabilityDataForAdding'
                )
            )
        if not os.path.exists(os.path.join(output_folder, simulation_folder, 'SiteRequirementDataForMultiply')):
            os.makedirs(os.path.join(
                output_folder, 
                simulation_folder,
                'SiteRequirementDataForMultiply'
                )
            )

        # field list of the zonal statistics
        zonal_statistic_field_names = []

        # normalize and weight 01 -flow accumulation
        if flow_accumulation_01:
            flow_accumulation_01_weighted_path = os.path.join(
                output_folder,
                simulation_folder, 
                'SubbasinDataForZonalStatistics',
                '01_flow_weighted.tif'
            )
            flow_accumulation_01_clipped_path = os.path.join(
                output_folder,
                simulation_folder, 
                'SiteRequirementDataForMultiply',
                '01_flow_clipped.tif'
            )
            flow_accumulation_01_clipped = processing.run("gdal:cliprasterbymasklayer", 
                {
                    'INPUT': flow_accumulation_01,
                    'MASK': wadi_subbasin.sourceName(),
                    'SOURCE_CRS':None,
                    'TARGET_CRS':None,
                    'NODATA':None,
                    'ALPHA_BAND':False,
                    'CROP_TO_CUTLINE':True,
                    'KEEP_RESOLUTION':False,
                    'SET_RESOLUTION':False,
                    'X_RESOLUTION':None,
                    'Y_RESOLUTION':None,
                    'MULTITHREADING':False,
                    'OPTIONS':'',
                    'DATA_TYPE':0,
                    'EXTRA':'',
                    'OUTPUT':flow_accumulation_01_clipped_path
                }, context=context, feedback=feedback)
            rmin, rmax = get_raster_stats(flow_accumulation_01_clipped_path)
            formula = f'((A-{rmin})/({rmax}-{rmin}))*{flow_accumulation_weight_01}'
            flow_accumulation_01_weighted = processing.run("gdal:rastercalculator", 
                    {
                        'INPUT_A': flow_accumulation_01_clipped['OUTPUT'],
                        'BAND_A':1, 
                        'FORMULA': formula,
                        'NO_DATA':None,
                        'RTYPE':5,
                        'OPTIONS':'',
                        'EXTRA':'',
                        'OUTPUT':flow_accumulation_01_weighted_path
                    }, context=context, feedback=feedback)
            feedback.setProgressText('Weighting Flow Accumulation...')
        
            # flow accumulation zonal statistics
            processing.run("qgis:zonalstatistics", 
                {
                    'INPUT_RASTER': flow_accumulation_01_weighted['OUTPUT'],
                    'RASTER_BAND':1,
                    'INPUT_VECTOR': wadi_subbasin_copy,
                    'COLUMN_PREFIX':'01_flowa',
                    'STATISTICS': [6] # 6 is the maximum
                    }
                )
            zonal_statistic_field_names.append('01_flowamax')
            feedback.setProgressText('Calculating Zonal Statistics of Flow Accumulation...')
            
        # normalize and weight 02 subbasin slope
        if subbasin_slope_02:
            subbasin_slope_02_weighted_path = os.path.join(
                output_folder,
                simulation_folder, 
                'SubbasinDataForZonalStatistics',
                '02_subbasin_slope_weighted.tif'
            )
            subbasin_slope_02_clipped_path = os.path.join(
                output_folder,
                simulation_folder, 
                'SiteRequirementDataForMultiply',
                '02_subbasin_slope_clipped.tif'
            )
            subbasin_slope_02_clipped = processing.run("gdal:cliprasterbymasklayer", 
                {
                    'INPUT': subbasin_slope_02,
                    'MASK': wadi_subbasin.sourceName(),
                    'SOURCE_CRS':None,
                    'TARGET_CRS':None,
                    'NODATA':None,
                    'ALPHA_BAND':False,
                    'CROP_TO_CUTLINE':True,
                    'KEEP_RESOLUTION':False,
                    'SET_RESOLUTION':False,
                    'X_RESOLUTION':None,
                    'Y_RESOLUTION':None,
                    'MULTITHREADING':False,
                    'OPTIONS':'',
                    'DATA_TYPE':0,
                    'EXTRA':'',
                    'OUTPUT': subbasin_slope_02_clipped_path
                }, context=context, feedback=feedback)
            rmin, rmax = get_raster_stats(subbasin_slope_02_clipped_path)
            formula = f'((A-{rmin})/({rmax}-{rmin}))*{subbasin_slope_weight_02}'
            subbasin_slope_02_weighted = processing.run("gdal:rastercalculator", 
                    {
                        'INPUT_A': subbasin_slope_02_clipped['OUTPUT'],
                        'BAND_A':1, 
                        'FORMULA': formula,
                        'NO_DATA':None,
                        'RTYPE':5,
                        'OPTIONS':'',
                        'EXTRA':'',
                        'OUTPUT':subbasin_slope_02_weighted_path
                    }, context=context, feedback=feedback)
            feedback.setProgressText('Weighting Slope...')

            # slope zonal statistics
            processing.run("qgis:zonalstatistics", 
                {
                    'INPUT_RASTER': subbasin_slope_02_weighted['OUTPUT'],
                    'RASTER_BAND':1,
                    'INPUT_VECTOR': wadi_subbasin_copy,
                    'COLUMN_PREFIX':'02_slope',
                    'STATISTICS':[2] # 2 is the mean
                    }
                )
            zonal_statistic_field_names.append('02_slopemean')
            feedback.setProgressText('Calculating Zonal Statistics of Slope...')         
        
        # normalize and weight 03 maximum flow lenght
        if max_flow_lenght_03:
            max_flow_lenght_03_weighted_path = os.path.join(
                output_folder,
                simulation_folder,
                'SubbasinDataForZonalStatistics',
                '03_max_flow_lenght_weighted.tif'
            )
            max_flow_lenght_03_clipped_path = os.path.join(
                output_folder,
                simulation_folder,
                'SiteRequirementDataForMultiply',
                '03_max_flow_lenght_clipped.tif'
            )
            max_flow_lenght_03_clipped = processing.run("gdal:cliprasterbymasklayer", 
                {
                    'INPUT': max_flow_lenght_03,
                    'MASK': wadi_subbasin.sourceName(),
                    'SOURCE_CRS':None,
                    'TARGET_CRS':None,
                    'NODATA':None,
                    'ALPHA_BAND':False,
                    'CROP_TO_CUTLINE':True,
                    'KEEP_RESOLUTION':False,
                    'SET_RESOLUTION':False,
                    'X_RESOLUTION':None,
                    'Y_RESOLUTION':None,
                    'MULTITHREADING':False,
                    'OPTIONS':'',
                    'DATA_TYPE':0,
                    'EXTRA':'',
                    'OUTPUT':max_flow_lenght_03_clipped_path
                }, context=context, feedback=feedback)
            rmin, rmax = get_raster_stats(max_flow_lenght_03_clipped_path)
            formula = f'((A-{rmin})/({rmax}-{rmin}))*{max_flow_lenght_weight_03}'
            max_flow_lenght_03_weighted = processing.run("gdal:rastercalculator", 
                    {
                        'INPUT_A': max_flow_lenght_03_clipped['OUTPUT'],
                        'BAND_A':1, 
                        'FORMULA': formula,
                        'NO_DATA':None,
                        'RTYPE':5,
                        'OPTIONS':'',
                        'EXTRA':'',
                        'OUTPUT':max_flow_lenght_03_weighted_path
                    }, context=context, feedback=feedback)
            feedback.setProgressText('Weighting Max flow Length...')
            # max flow lenght zonal statistics
            processing.run("qgis:zonalstatistics", 
                {
                    'INPUT_RASTER': max_flow_lenght_03_weighted['OUTPUT'],
                    'RASTER_BAND':1,
                    'INPUT_VECTOR': wadi_subbasin_copy,
                    'COLUMN_PREFIX':'03_flowl',
                    'STATISTICS':[6] # 6 is the maximum
                    }
                )
            zonal_statistic_field_names.append('03_flowlmax')
            feedback.setProgressText('Calculating Zonal Statistics of Max Flow Length...')                  
        
        # normalize and weight 04 precipitation height
        if precipitation_height_04:
            precipitation_height_04_weighted_path = os.path.join(
                output_folder,
                simulation_folder,
                'SubbasinDataForZonalStatistics',
                '04_precipitation_height_weighted.tif'
            )
            precipitation_height_04_clipped_path = os.path.join(
                output_folder,
                simulation_folder,
                'SiteRequirementDataForMultiply',
                '04_precipitation_height_clipped.tif'
            )
            precipitation_height_04_clipped = processing.run("gdal:cliprasterbymasklayer", 
                {
                    'INPUT': precipitation_height_04,
                    'MASK': wadi_subbasin.sourceName(),
                    'SOURCE_CRS':None,
                    'TARGET_CRS':None,
                    'NODATA':None,
                    'ALPHA_BAND':False,
                    'CROP_TO_CUTLINE':True,
                    'KEEP_RESOLUTION':False,
                    'SET_RESOLUTION':False,
                    'X_RESOLUTION':None,
                    'Y_RESOLUTION':None,
                    'MULTITHREADING':False,
                    'OPTIONS':'',
                    'DATA_TYPE':0,
                    'EXTRA':'',
                    'OUTPUT': precipitation_height_04_clipped_path
                }, context=context, feedback=feedback)
            rmin, rmax = get_raster_stats(precipitation_height_04_clipped_path)
            formula = f'((A-{rmin})/({rmax}-{rmin}))*{precipitation_height_weight_04}'
            precipitation_height_04_weighted = processing.run("gdal:rastercalculator", 
                    {
                        'INPUT_A': precipitation_height_04_clipped['OUTPUT'],
                        'BAND_A':1,
                        'FORMULA': formula, 
                        'NO_DATA':None,
                        'RTYPE':5,
                        'OPTIONS':'',
                        'EXTRA':'',
                        'OUTPUT':precipitation_height_04_weighted_path
                    }, context=context, feedback=feedback)
            feedback.setProgressText('Weighting Precipitation...')

            # precipitation height zonal statistics
            processing.run("qgis:zonalstatistics", 
                {
                    'INPUT_RASTER': precipitation_height_04_weighted['OUTPUT'],
                    'RASTER_BAND':1,
                    'INPUT_VECTOR': wadi_subbasin_copy,
                    'COLUMN_PREFIX':'04_prhe',
                    'STATISTICS':[2] # 2 is the mean
                    }
                )
            zonal_statistic_field_names.append('04_prhemean')
            feedback.setProgressText('Calculating Zonal Statistics of Precipitation...')         
                
        # normalize and weight 05 precipitation intensity
        if precipitation_intensity_05:
            precipitation_intensity_05_weighted_path = os.path.join(
                output_folder,
                simulation_folder,
                'SubbasinDataForZonalStatistics',
                '05_precipitation_intensity_weighted.tif'
            )
            precipitation_intensity_05_clipped_path = os.path.join(
                output_folder,
                simulation_folder,
                'SiteRequirementDataForMultiply',
                '05_precipitation_intensity_clipped.tif'
            )
            precipitation_intensity_05_clipped = processing.run("gdal:cliprasterbymasklayer", 
                {
                    'INPUT': precipitation_intensity_05,
                    'MASK': wadi_subbasin.sourceName(),
                    'SOURCE_CRS':None,
                    'TARGET_CRS':None,
                    'NODATA':None,
                    'ALPHA_BAND':False,
                    'CROP_TO_CUTLINE':True,
                    'KEEP_RESOLUTION':False,
                    'SET_RESOLUTION':False,
                    'X_RESOLUTION':None,
                    'Y_RESOLUTION':None,
                    'MULTITHREADING':False,
                    'OPTIONS':'',
                    'DATA_TYPE':0,
                    'EXTRA':'',
                    'OUTPUT': precipitation_intensity_05_clipped_path
                }, context=context, feedback=feedback)
            rmin, rmax = get_raster_stats(precipitation_intensity_05_clipped_path)
            formula = f'((A-{rmin})/({rmax}-{rmin}))*{precipitation_intensity_weight_05}'
            precipitation_intensity_05_weighted = processing.run("gdal:rastercalculator", 
                {
                    'INPUT_A': precipitation_intensity_05_clipped['OUTPUT'],
                    'BAND_A':1, 
                    'FORMULA': formula,
                    'NO_DATA':None,
                    'RTYPE':5,
                    'OPTIONS':'',
                    'EXTRA':'',
                    'OUTPUT':precipitation_intensity_05_weighted_path
                }, context=context, feedback=feedback)
            feedback.setProgressText('Weighting Intensity...')

            # precipitation intensity zonal statistics
            processing.run("qgis:zonalstatistics", 
                {
                    'INPUT_RASTER': precipitation_intensity_05_weighted['OUTPUT'],
                    'RASTER_BAND':1,
                    'INPUT_VECTOR': wadi_subbasin_copy,
                    'COLUMN_PREFIX':'05_prin',
                    'STATISTICS':[2] # 2 is the mean
                    }
                )
            zonal_statistic_field_names.append('05_prinmean')
            feedback.setProgressText('Calculating Zonal Statistics of Intensity...')
        
        # normalize and weight 06 dry period vegentation
        if dry_period_06:
            dry_period_06_weighted_path = os.path.join(
                output_folder,
                simulation_folder,
                'SubbasinDataForZonalStatistics',
                '06_dry_period_weighted.tif'
            )
            dry_period_06_clipped_path = os.path.join(
                output_folder,
                simulation_folder,
                'SiteRequirementDataForMultiply',
                '06_dry_period_clipped.tif'
            )
            dry_period_06_weighted_clipped = processing.run("gdal:cliprasterbymasklayer", 
                {
                    'INPUT': dry_period_06,
                    'MASK': wadi_subbasin.sourceName(),
                    'SOURCE_CRS':None,
                    'TARGET_CRS':None,
                    'NODATA':None,
                    'ALPHA_BAND':False,
                    'CROP_TO_CUTLINE':True,
                    'KEEP_RESOLUTION':False,
                    'SET_RESOLUTION':False,
                    'X_RESOLUTION':None,
                    'Y_RESOLUTION':None,
                    'MULTITHREADING':False,
                    'OPTIONS':'',
                    'DATA_TYPE':0,
                    'EXTRA':'',
                    'OUTPUT': dry_period_06_clipped_path
                }, context=context, feedback=feedback)
            rmin, rmax = get_raster_stats(dry_period_06_clipped_path)
            formula = f'((A-{rmin})/({rmax}-{rmin}))*{dry_period_weight_06}'
            dry_period_06_weighted = processing.run("gdal:rastercalculator", 
                {
                    'INPUT_A': dry_period_06_weighted_clipped['OUTPUT'],
                    'BAND_A':1, 
                    'FORMULA': formula,
                    'NO_DATA':None,
                    'RTYPE':5,
                    'OPTIONS':'',
                    'EXTRA':'',
                    'OUTPUT': dry_period_06_weighted_path
                }, context=context, feedback=feedback)
            feedback.setProgressText('Weighting Dry Period...')

            # dry period zonal statistics
            processing.run("qgis:zonalstatistics", 
                {
                    'INPUT_RASTER': dry_period_06_weighted['OUTPUT'],
                    'RASTER_BAND':1,
                    'INPUT_VECTOR': wadi_subbasin_copy,
                    'COLUMN_PREFIX':'06_dry',
                    'STATISTICS':[2] # 2 is the mean
                    }
                )
            zonal_statistic_field_names.append('06_drymean')
            feedback.setProgressText('Calculating Zonal Statistics of Dry Period...')              
        
        # normalize and weight 07 temperature
        if temperature_07:
            temperature_07_weighted_path = os.path.join(
                output_folder,
                simulation_folder,
                'SubbasinDataForZonalStatistics',
                '07_temperature_weighted.tif'
            )
            temperature_07_clipped_path = os.path.join(
                output_folder,
                simulation_folder,
                'SiteRequirementDataForMultiply',
                '07_temperature_clipped.tif'
            )
            temperature_07_clipped = processing.run("gdal:cliprasterbymasklayer", 
                {
                    'INPUT': temperature_07,
                    'MASK': wadi_subbasin.sourceName(),
                    'SOURCE_CRS':None,
                    'TARGET_CRS':None,
                    'NODATA':None,
                    'ALPHA_BAND':False,
                    'CROP_TO_CUTLINE':True,
                    'KEEP_RESOLUTION':False,
                    'SET_RESOLUTION':False,
                    'X_RESOLUTION':None,
                    'Y_RESOLUTION':None,
                    'MULTITHREADING':False,
                    'OPTIONS':'',
                    'DATA_TYPE':0,
                    'EXTRA':'',
                    'OUTPUT': temperature_07_clipped_path
                }, context=context, feedback=feedback)
            rmin, rmax = get_raster_stats(temperature_07_clipped_path)
            formula = f'((A-{rmin})/({rmax}-{rmin}))*{temperature_weight_07}'
            temperature_07_weighted = processing.run("gdal:rastercalculator", 
                {'INPUT_A': temperature_07_clipped['OUTPUT'],
                'BAND_A':1, 
                'FORMULA': formula,
                'NO_DATA':None,
                'RTYPE':5,
                'OPTIONS':'',
                'EXTRA':'',
                'OUTPUT': temperature_07_weighted_path
                }, context=context, feedback=feedback)
            feedback.setProgressText('Weighting Temperature...')

            # temperature zonal statistics
            processing.run("qgis:zonalstatistics", 
                {
                    'INPUT_RASTER': temperature_07_weighted['OUTPUT'],
                    'RASTER_BAND':1,
                    'INPUT_VECTOR': wadi_subbasin_copy,
                    'COLUMN_PREFIX':'07_temp',
                    'STATISTICS':[2] # 2 is the mean
                    }
                )
            zonal_statistic_field_names.append('07_tempmean')
            feedback.setProgressText('Calculating Zonal Statistics of Temperature...')
        
        # normalize and weight 08 interception
        if interception_08:
            interception_08_weighted_path = os.path.join(
                output_folder,
                simulation_folder,
                'SubbasinDataForZonalStatistics',
                '08_interception_weighted.tif'
            )
            interception_08_clipped_path = os.path.join(
                output_folder,
                simulation_folder,
                'SiteRequirementDataForMultiply',
                '08_interception_clipped.tif'
            )
            interception_08_clipped = processing.run("gdal:cliprasterbymasklayer", 
                {
                    'INPUT': interception_08,
                    'MASK': wadi_subbasin.sourceName(),
                    'SOURCE_CRS':None,
                    'TARGET_CRS':None,
                    'NODATA':None,
                    'ALPHA_BAND':False,
                    'CROP_TO_CUTLINE':True,
                    'KEEP_RESOLUTION':False,
                    'SET_RESOLUTION':False,
                    'X_RESOLUTION':None,
                    'Y_RESOLUTION':None,
                    'MULTITHREADING':False,
                    'OPTIONS':'',
                    'DATA_TYPE':0,
                    'EXTRA':'',
                    'OUTPUT': interception_08_clipped_path
                }, context=context, feedback=feedback)
            rmin, rmax = get_raster_stats(interception_08_clipped_path)
            formula = f'((A-{rmin})/({rmax}-{rmin}))*{interception_weight_08}'
            interception_08_weighted = processing.run("gdal:rastercalculator", 
                {'INPUT_A': interception_08_clipped['OUTPUT'],
                'BAND_A':1, 
                'FORMULA': formula,
                'NO_DATA':None,
                'RTYPE':5,
                'OPTIONS':'',
                'EXTRA':'',
                'OUTPUT': interception_08_weighted_path
                }, context=context, feedback=feedback)
            feedback.setProgressText('Weighting Interception...')

            # interception zonal statistics
            processing.run("qgis:zonalstatistics", 
                {
                    'INPUT_RASTER': interception_08_weighted['OUTPUT'],
                    'RASTER_BAND':1,
                    'INPUT_VECTOR': wadi_subbasin_copy,
                    'COLUMN_PREFIX':'08_inter',
                    'STATISTICS':[2] # 2 is the mean
                    }
                )
            zonal_statistic_field_names.append('08_intermean')
            feedback.setProgressText('Calculating Zonal Statistics of Interception...')

        # normalize and weight 09 retention
        if retention_09:
            retention_09_weighted_path = os.path.join(
                output_folder,
                simulation_folder,
                'SubbasinDataForZonalStatistics',
                '09_retention_weighted.tif'
            )
            retention_09_clipped_path = os.path.join(
                output_folder,
                simulation_folder,
                'SiteRequirementDataForMultiply',
                '09_retention_clipped.tif'
            )
            retention_09_clipped = processing.run("gdal:cliprasterbymasklayer", 
                {
                    'INPUT': retention_09,
                    'MASK': wadi_subbasin.sourceName(),
                    'SOURCE_CRS':None,
                    'TARGET_CRS':None,
                    'NODATA':None,
                    'ALPHA_BAND':False,
                    'CROP_TO_CUTLINE':True,
                    'KEEP_RESOLUTION':False,
                    'SET_RESOLUTION':False,
                    'X_RESOLUTION':None,
                    'Y_RESOLUTION':None,
                    'MULTITHREADING':False,
                    'OPTIONS':'',
                    'DATA_TYPE':0,
                    'EXTRA':'',
                    'OUTPUT': retention_09_clipped_path
                }, context=context, feedback=feedback)
            rmin, rmax = get_raster_stats(retention_09_clipped_path)
            formula = f'((A-{rmin})/({rmax}-{rmin}))*{retention_weight_09}'
            retention_09_weighted = processing.run("gdal:rastercalculator", 
                {
                    'INPUT_A': retention_09_clipped['OUTPUT'],
                    'BAND_A':1,
                    'FORMULA': formula,
                    'NO_DATA':None,
                    'RTYPE':5,
                    'OPTIONS':'',
                    'EXTRA':'',
                    'OUTPUT': retention_09_weighted_path
                }, context=context, feedback=feedback)
            feedback.setProgressText('Weighting Retention...')

            # retention zonal statistics
            processing.run("qgis:zonalstatistics", 
                {
                    'INPUT_RASTER': retention_09_weighted['OUTPUT'],
                    'RASTER_BAND':1,
                    'INPUT_VECTOR': wadi_subbasin_copy,
                    'COLUMN_PREFIX':'09_reten',
                    'STATISTICS':[2] # 2 is the mean
                    }
                )
            zonal_statistic_field_names.append('09_retenmean')
            feedback.setProgressText('Calculating Zonal Statistics of Retention...')

        # normalize and weight 10 sand content
        if sand_content_10:
            sand_content_10_weighted_path = os.path.join(
                output_folder,
                simulation_folder,
                'SubbasinDataForZonalStatistics',
                '10_and_content_weighted.tif'
            )
            sand_content_10_clipped_path = os.path.join(
                output_folder,
                simulation_folder,
                'SiteRequirementDataForMultiply',
                '10_and_content_clipped.tif'
            )
            sand_content_10_clipped = processing.run("gdal:cliprasterbymasklayer", 
                {
                    'INPUT': sand_content_10,
                    'MASK': wadi_subbasin.sourceName(),
                    'SOURCE_CRS':None,
                    'TARGET_CRS':None,
                    'NODATA':None,
                    'ALPHA_BAND':False,
                    'CROP_TO_CUTLINE':True,
                    'KEEP_RESOLUTION':False,
                    'SET_RESOLUTION':False,
                    'X_RESOLUTION':None,
                    'Y_RESOLUTION':None,
                    'MULTITHREADING':False,
                    'OPTIONS':'',
                    'DATA_TYPE':0,
                    'EXTRA':'',
                    'OUTPUT': sand_content_10_clipped_path
                }, context=context, feedback=feedback)
            rmin, rmax = get_raster_stats(sand_content_10_clipped_path)
            formula = f'((A-{rmin})/({rmax}-{rmin}))*{sand_content_weight_10}'
            sand_content_10_weighted = processing.run("gdal:rastercalculator", 
                {
                    'INPUT_A': sand_content_10_clipped['OUTPUT'],
                    'BAND_A':1,
                    'FORMULA': formula,
                    'NO_DATA':None,
                    'RTYPE':5,
                    'OPTIONS':'',
                    'EXTRA':'',
                    'OUTPUT': sand_content_10_weighted_path
                }, context=context, feedback=feedback)
            feedback.setProgressText('Weighting Sand Content...')

            # sand content zonal statistics
            processing.run("qgis:zonalstatistics", 
                {
                    'INPUT_RASTER': sand_content_10_weighted['OUTPUT'],
                    'RASTER_BAND':1,
                    'INPUT_VECTOR': wadi_subbasin_copy,
                    'COLUMN_PREFIX':'10_sand',
                    'STATISTICS':[2] # 2 is the mean
                    }
                )
            zonal_statistic_field_names.append('10_sandmean')
            feedback.setProgressText('Calculating Zonal Statistics of Sand Content...')

        # normalize and weight 11 stone content
        if stone_content_11:
            stone_content_11_weighted_path = os.path.join(
                output_folder,
                simulation_folder,
                'SubbasinDataForZonalStatistics',
                '11_stone_content_weighted.tif'
            )
            stone_content_11_clipped_path = os.path.join(
                output_folder,
                simulation_folder,
                'SiteRequirementDataForMultiply',
                '11_stone_content_clipped.tif'
            )
            stone_content_11_clipped = processing.run("gdal:cliprasterbymasklayer", 
                {
                    'INPUT': stone_content_11,
                    'MASK': wadi_subbasin.sourceName(),
                    'SOURCE_CRS':None,
                    'TARGET_CRS':None,
                    'NODATA':None,
                    'ALPHA_BAND':False,
                    'CROP_TO_CUTLINE':True,
                    'KEEP_RESOLUTION':False,
                    'SET_RESOLUTION':False,
                    'X_RESOLUTION':None,
                    'Y_RESOLUTION':None,
                    'MULTITHREADING':False,
                    'OPTIONS':'',
                    'DATA_TYPE':0,
                    'EXTRA':'',
                    'OUTPUT': stone_content_11_clipped_path
                }, context=context, feedback=feedback)
            rmin, rmax = get_raster_stats(stone_content_11_clipped_path)
            formula = f'((A-{rmin})/({rmax}-{rmin}))*{stone_content_weight_11}'
            stone_content_11_weighted = processing.run("gdal:rastercalculator", 
                {
                    'INPUT_A': stone_content_11_clipped['OUTPUT'],
                    'BAND_A':1,
                    'FORMULA': formula,
                    'NO_DATA':None,
                    'RTYPE':5,
                    'OPTIONS':'',
                    'EXTRA':'',
                    'OUTPUT': stone_content_11_weighted_path
                }, context=context, feedback=feedback)
            feedback.setProgressText('Weighting Stone Content...')
            
            # stone content zonal statistics
            processing.run("qgis:zonalstatistics", 
                {
                    'INPUT_RASTER': stone_content_11_weighted['OUTPUT'],
                    'RASTER_BAND':1,
                    'INPUT_VECTOR': wadi_subbasin_copy,
                    'COLUMN_PREFIX':'11_stone',
                    'STATISTICS':[2] # 2 is the mean
                    }
                )
            zonal_statistic_field_names.append('11_stonemean')
            feedback.setProgressText('Calculating Zonal Statistics of Stone Content...')

        # normalize and weight 12 subsurface permeability
        if subsurface_permeability_12:
            subsurface_permeability_12_weighted_path = os.path.join(
                output_folder,
                simulation_folder,
                'SubbasinDataForZonalStatistics',
                '12_subsurface_permeability_weighted.tif'
            )
            subsurface_permeability_12_clipped_path = os.path.join(
                output_folder,
                simulation_folder,
                'SiteRequirementDataForMultiply',
                '12_subsurface_permeability_clipped.tif'
            )
            subsurface_permeability_12_clipped = processing.run("gdal:cliprasterbymasklayer", 
                {
                    'INPUT': subsurface_permeability_12,
                    'MASK': wadi_subbasin.sourceName(),
                    'SOURCE_CRS':None,
                    'TARGET_CRS':None,
                    'NODATA':None,
                    'ALPHA_BAND':False,
                    'CROP_TO_CUTLINE':True,
                    'KEEP_RESOLUTION':False,
                    'SET_RESOLUTION':False,
                    'X_RESOLUTION':None,
                    'Y_RESOLUTION':None,
                    'MULTITHREADING':False,
                    'OPTIONS':'',
                    'DATA_TYPE':0,
                    'EXTRA':'',
                    'OUTPUT': subsurface_permeability_12_clipped_path
                }, context=context, feedback=feedback)
            rmin, rmax = get_raster_stats(subsurface_permeability_12_clipped_path)
            formula = f'((A-{rmin})/({rmax}-{rmin}))*{subsurface_permeability_weight_12}'
            subsurface_permeability_12_weighted = processing.run("gdal:rastercalculator", 
                {
                    'INPUT_A': subsurface_permeability_12_clipped['OUTPUT'],
                    'BAND_A':1,
                    'FORMULA': formula,
                    'NO_DATA':None,
                    'RTYPE':5,
                    'OPTIONS':'',
                    'EXTRA':'',
                    'OUTPUT': subsurface_permeability_12_weighted_path
                }, context=context, feedback=feedback)
            feedback.setProgressText('Weighting Subsurface Permeability...')
            
            # subsurface zonal statistics
            processing.run("qgis:zonalstatistics", 
                {
                    'INPUT_RASTER': subsurface_permeability_12_weighted['OUTPUT'],
                    'RASTER_BAND':1,
                    'INPUT_VECTOR': wadi_subbasin_copy,
                    'COLUMN_PREFIX':'12_subs',
                    'STATISTICS':[2] # 2 is the mean
                    }
                )
            zonal_statistic_field_names.append('12_subsmean')
            feedback.setProgressText('Calculating Zonal Statistics of Subsurface Permeability...')

        # normalize and weight 13 infiltration
        if infiltration_13:
            infiltration_13_weighted_path = os.path.join(
                output_folder,
                simulation_folder,
                'SubbasinDataForZonalStatistics',
                '13_infiltration_weighted.tif'
            )
            infiltration_13_clipped_path = os.path.join(
                output_folder,
                simulation_folder,
                'SiteRequirementDataForMultiply',
                '13_infiltration_clipped.tif'
            )
            infiltration_13_clipped = processing.run("gdal:cliprasterbymasklayer", 
                {
                    'INPUT': infiltration_13,
                    'MASK': wadi_subbasin.sourceName(),
                    'SOURCE_CRS':None,
                    'TARGET_CRS':None,
                    'NODATA':None,
                    'ALPHA_BAND':False,
                    'CROP_TO_CUTLINE':True,
                    'KEEP_RESOLUTION':False,
                    'SET_RESOLUTION':False,
                    'X_RESOLUTION':None,
                    'Y_RESOLUTION':None,
                    'MULTITHREADING':False,
                    'OPTIONS':'',
                    'DATA_TYPE':0,
                    'EXTRA':'',
                    'OUTPUT': infiltration_13_clipped_path
                }, context=context, feedback=feedback)
            rmin, rmax = get_raster_stats(infiltration_13_clipped_path)
            formula = f'((A-{rmin})/({rmax}-{rmin}))*{infiltration_weight_13}'
            infiltration_13_weighted = processing.run("gdal:rastercalculator", 
                {'INPUT_A': infiltration_13_clipped['OUTPUT'],
                'BAND_A':1,
                'FORMULA': formula,
                'NO_DATA':None,
                'RTYPE':5,
                'OPTIONS':'',
                'EXTRA':'',
                'OUTPUT': infiltration_13_weighted_path
                }, context=context, feedback=feedback)
            feedback.setProgressText('Weighting Infiltration...')

            # infiltration zonal statistics
            processing.run("qgis:zonalstatistics", 
                {
                    'INPUT_RASTER': infiltration_13_weighted['OUTPUT'],
                    'RASTER_BAND':1,
                    'INPUT_VECTOR': wadi_subbasin_copy,
                    'COLUMN_PREFIX':'13_infil',
                    'STATISTICS':[2] # 2 is the mean
                    }
                )
            zonal_statistic_field_names.append('13_infilmean')
            feedback.setProgressText('Calculating Zonal Statistics of Infiltration...')

        # normalize and weight 14 soil salinity
        if soil_salinity_14:
            soil_salinity_14_weighted_path = os.path.join(
                output_folder,
                simulation_folder,
                'SubbasinDataForZonalStatistics',
                '14_soil_salinity_weighted.tif'
            )
            soil_salinity_14_clipped_path = os.path.join(
                output_folder,
                simulation_folder,
                'SiteRequirementDataForMultiply',
                '14_soil_salinity_clipped.tif'
            )
            soil_salinity_14_clipped = processing.run("gdal:cliprasterbymasklayer", 
                {
                    'INPUT': soil_salinity_14,
                    'MASK': wadi_subbasin.sourceName(),
                    'SOURCE_CRS':None,
                    'TARGET_CRS':None,
                    'NODATA':None,
                    'ALPHA_BAND':False,
                    'CROP_TO_CUTLINE':True,
                    'KEEP_RESOLUTION':False,
                    'SET_RESOLUTION':False,
                    'X_RESOLUTION':None,
                    'Y_RESOLUTION':None,
                    'MULTITHREADING':False,
                    'OPTIONS':'',
                    'DATA_TYPE':0,
                    'EXTRA':'',
                    'OUTPUT': soil_salinity_14_clipped_path
                }, context=context, feedback=feedback)
            rmin, rmax = get_raster_stats(soil_salinity_14_clipped_path)
            formula = f'((A-{rmin})/({rmax}-{rmin}))*{soil_salinity_weight_14}'
            soil_salinity_14_weighted = processing.run("gdal:rastercalculator", 
                {
                    'INPUT_A': soil_salinity_14_clipped['OUTPUT'],
                    'BAND_A':1,
                    'FORMULA': formula,
                    'NO_DATA':None,
                    'RTYPE':5,
                    'OPTIONS':'',
                    'EXTRA':'',
                    'OUTPUT': soil_salinity_14_weighted_path
                }, context=context, feedback=feedback)
            feedback.setProgressText('Weighting Soil Salinity...')

            # soil salinity zonal statistics
            processing.run("qgis:zonalstatistics", 
                {
                    'INPUT_RASTER': soil_salinity_14_weighted['OUTPUT'],
                    'RASTER_BAND':1,
                    'INPUT_VECTOR': wadi_subbasin_copy,
                    'COLUMN_PREFIX':'14_soils',
                    'STATISTICS':[2] # 2 is the mean
                    }
                )
            zonal_statistic_field_names.append('14_soilsmean')
            feedback.setProgressText('Calculating Zonal Statistics of Soil Salinity...')
        
        # context.temporaryLayerStore().addMapLayer(wadi_subbasin_copy)
        # context.addLayerToLoadOnCompletion(wadi_subbasin_copy.source(), context.LayerDetails(
        #     name='wadi_subbasin_zonal_statistics',
        #     project=context.project()
        # ))
        

        # write into the output folder the materialized copy of wadi_subbasin_copy layer in the folder
        # QgsVectorFileWriter.writeAsVectorFormat(
        #     wadi_subbasin_copy, 
        #     os.path.join(
        #         output_folder,
        #         simulation_folder, 
        #         'SubbasinDataForZonalStatistics',
        #         'wadi_subbasin_zonal_statistics.shp'
        #     ), 
        #     'utf8', 
        #     wadi_subbasin_copy.crs(), 
        #     'ESRI Shapefile'
        # )

        # add the materialized copy of the wadi subbasin layer to the context 
        # and load it into the legend
        # context.temporaryLayerStore().addMapLayer(wadi_subbasin_copy)
        # context.addLayerToLoadOnCompletion(wadi_subbasin_copy.source(), context.LayerDetails(
        #     name='wadi_subbasin_zonal_statistics',
        #     project=context.project()
        # ))


        # SECOND TAB - SITE PARAMETERS

        site_requirements_raster_list = []
        # append stream location 15 raster layer as it is
        stream_location_15_clipped_path = os.path.join(
                output_folder,
                simulation_folder,
                'SiteRequirementDataForMultiply',
                '15_stream_location_clipped.tif'
            )
        stream_location_15_clipped = processing.run("gdal:cliprasterbymasklayer", 
            {
                'INPUT': stream_location_15,
                'MASK': wadi_subbasin.sourceName(),
                'SOURCE_CRS':None,
                'TARGET_CRS':None,
                'NODATA':None,
                'ALPHA_BAND':False,
                'CROP_TO_CUTLINE':True,
                'KEEP_RESOLUTION':False,
                'SET_RESOLUTION':False,
                'X_RESOLUTION':None,
                'Y_RESOLUTION':None,
                'MULTITHREADING':False,
                'OPTIONS':'',
                'DATA_TYPE':0,
                'EXTRA':'',
                'OUTPUT': stream_location_15_clipped_path
            }, context=context, feedback=feedback)
        site_requirements_raster_list.append(stream_location_15_clipped['OUTPUT'])

        # river banks height 16
        if river_banks_height_16:
            river_banks_height_16_clipped_path = os.path.join(
                output_folder,
                simulation_folder,
                'SiteRequirementDataForMultiply',
                '16_river_banks_height_clipped.tif'
            )
            river_banks_height_16_clipped = processing.run("gdal:cliprasterbymasklayer", 
                {
                    'INPUT': river_banks_height_16,
                    'MASK': wadi_subbasin.sourceName(),
                    'SOURCE_CRS':None,
                    'TARGET_CRS':None,
                    'NODATA':None,
                    'ALPHA_BAND':False,
                    'CROP_TO_CUTLINE':True,
                    'KEEP_RESOLUTION':False,
                    'SET_RESOLUTION':False,
                    'X_RESOLUTION':None,
                    'Y_RESOLUTION':None,
                    'MULTITHREADING':False,
                    'OPTIONS':'',
                    'DATA_TYPE':0,
                    'EXTRA':'',
                    'OUTPUT': river_banks_height_16_clipped_path
                }, context=context, feedback=feedback)
            river_banks_height_16_calculation = processing.run("gdal:rastercalculator", 
                {
                    'INPUT_A': river_banks_height_16_clipped['OUTPUT'],
                    'BAND_A':1, 
                    'FORMULA': f"A*{river_banks_heigth_min_16}",
                    'NO_DATA':None,
                    'RTYPE':5,
                    'OPTIONS':'',
                    'EXTRA':'',
                    'OUTPUT':'TEMPORARY_OUTPUT'
                }, context=context, feedback=feedback)['OUTPUT']
            site_requirements_raster_list.append(river_banks_height_16_calculation)
        feedback.setProgressText('Weighting River Banks Height...')

        # river banks widht 17
        if river_banks_width_17:
            river_banks_width_17_clipped_path = os.path.join(
                output_folder,
                simulation_folder,
                'SiteRequirementDataForMultiply',
                '17_river_banks_width_clipped.tif'
            )
            river_banks_width_17_clipped = processing.run("gdal:cliprasterbymasklayer", 
                {
                    'INPUT': river_banks_width_17,
                    'MASK': wadi_subbasin.sourceName(),
                    'SOURCE_CRS':None,
                    'TARGET_CRS':None,
                    'NODATA':None,
                    'ALPHA_BAND':False,
                    'CROP_TO_CUTLINE':True,
                    'KEEP_RESOLUTION':False,
                    'SET_RESOLUTION':False,
                    'X_RESOLUTION':None,
                    'Y_RESOLUTION':None,
                    'MULTITHREADING':False,
                    'OPTIONS':'',
                    'DATA_TYPE':0,
                    'EXTRA':'',
                    'OUTPUT': river_banks_width_17_clipped_path
                }, context=context, feedback=feedback)
            river_banks_width_17_calculation = processing.run("gdal:rastercalculator", 
                {
                    'INPUT_A': river_banks_width_17_clipped['OUTPUT'],
                    'BAND_A':1, 
                    'FORMULA': f"A*{river_banks_width_min_17}",
                    'NO_DATA':None,
                    'RTYPE':5,
                    'OPTIONS':'',
                    'EXTRA':'',
                    'OUTPUT':'TEMPORARY_OUTPUT'
                }, context=context, feedback=feedback)['OUTPUT']
            site_requirements_raster_list.append(river_banks_width_17_calculation)
        feedback.setProgressText('Weighting River Banks Width...')



        # slope 18 calculation
        if slope_18:
            slope_18_clipped_path = os.path.join(
                output_folder,
                simulation_folder,
                'SiteRequirementDataForMultiply',
                '18_slope_clipped.tif'
            )
            slope_18_clipped = processing.run("gdal:cliprasterbymasklayer", 
                {
                    'INPUT': slope_18,
                    'MASK': wadi_subbasin.sourceName(),
                    'SOURCE_CRS':None,
                    'TARGET_CRS':None,
                    'NODATA':None,
                    'ALPHA_BAND':False,
                    'CROP_TO_CUTLINE':True,
                    'KEEP_RESOLUTION':False,
                    'SET_RESOLUTION':False,
                    'X_RESOLUTION':None,
                    'Y_RESOLUTION':None,
                    'MULTITHREADING':False,
                    'OPTIONS':'',
                    'DATA_TYPE':0,
                    'EXTRA':'',
                    'OUTPUT': slope_18_clipped_path
                }, context=context, feedback=feedback)
            formula = f"logical_and((A*100)>{slope_18_min}, (A*100)<{slope_18_max})"
            slope_18_calculation = processing.run("gdal:rastercalculator", 
                {
                    'INPUT_A': slope_18_clipped['OUTPUT'],
                    'BAND_A':1, 
                    'FORMULA': formula,
                    'NO_DATA':None,
                    'RTYPE':5,
                    'OPTIONS':'',
                    'EXTRA':'',
                    'OUTPUT':'TEMPORARY_OUTPUT'
                }, context=context, feedback=feedback)['OUTPUT']
            site_requirements_raster_list.append(slope_18_calculation)
            feedback.setProgressText('Calculating Slope Suitability...')

        # append auitable permeability 19 raster layer as it is
        if suitable_permeability_19:
            suitable_permeability_19_clipped_path = os.path.join(
                output_folder,
                simulation_folder,
                'SiteRequirementDataForMultiply',
                '19_suitable_permeability_clipped.tif'
            )
            suitable_permeability_19_clipped = processing.run("gdal:cliprasterbymasklayer", 
                {
                    'INPUT': suitable_permeability_19,
                    'MASK': wadi_subbasin.sourceName(),
                    'SOURCE_CRS':None,
                    'TARGET_CRS':None,
                    'NODATA':None,
                    'ALPHA_BAND':False,
                    'CROP_TO_CUTLINE':True,
                    'KEEP_RESOLUTION':False,
                    'SET_RESOLUTION':False,
                    'X_RESOLUTION':None,
                    'Y_RESOLUTION':None,
                    'MULTITHREADING':False,
                    'OPTIONS':'',
                    'DATA_TYPE':0,
                    'EXTRA':'',
                    'OUTPUT': suitable_permeability_19_clipped_path
                }, context=context, feedback=feedback)
            site_requirements_raster_list.append(suitable_permeability_19_clipped['OUTPUT'])

        # extract suitable shallow groundwater ec
        if shallow_groundwater_ec_20:
            shallow_groundwater_ec_20_clipped = processing.run("native:extractbylocation", 
                {
                    'INPUT': shallow_groundwater_ec_20.sourceName(),
                    'PREDICATE': [0],
                    'INTERSECT': wadi_subbasin.sourceName(),
                    'OUTPUT': 'TEMPORARY_OUTPUT'
                }, context=context, feedback=feedback)
            # save the clipped layer into the folder
            QgsVectorFileWriter.writeAsVectorFormat(
                shallow_groundwater_ec_20_clipped['OUTPUT'], 
                os.path.join(
                    output_folder,
                    simulation_folder,
                    'SiteRequirementDataForMultiply',
                    '20_shallow_groundwater_ec.shp'
                ),
                'utf8', 
                shallow_groundwater_ec_20_clipped['OUTPUT'].crs(), 
                'ESRI Shapefile'
            )
            # make the buffer ONLY if there are features
            if shallow_groundwater_ec_20_clipped['OUTPUT'].featureCount():
                groundwater_20_extracted = processing.run("native:extractbyexpression", 
                    {
                        'INPUT': shallow_groundwater_ec_20_clipped['OUTPUT'],
                        'EXPRESSION':f"{shallow_groundwater_ec_field_20} > {shallow_groundwater_ec_threshold_20}",
                        'OUTPUT':'TEMPORARY_OUTPUT'
                    }, context=context, feedback=feedback)['OUTPUT']
                feedback.setProgressText('Filtering Groundwater Threshold...')
                
                groundwater_20_buffer = processing.run("native:buffer", 
                    {
                        'INPUT': groundwater_20_extracted,
                        'DISTANCE': shallow_groundwater_ec_distance_20,
                        'SEGMENTS':5,
                        'END_CAP_STYLE':0,
                        'JOIN_STYLE':0,
                        'MITER_LIMIT':2,
                        'DISSOLVE':True,
                        'OUTPUT':'TEMPORARY_OUTPUT'
                    }, context=context, feedback=feedback)['OUTPUT']
                feedback.setProgressText('Creating Groundwater Buffer...')

                groundwater_20_buffer_difference = processing.run("native:difference", 
                    {
                        'INPUT': wadi_subbasin.sourceName(),
                        'OVERLAY': groundwater_20_buffer,
                        'OUTPUT':'TEMPORARY_OUTPUT'
                    }, context=context, feedback=feedback)['OUTPUT']
                
                
                groundwater_20_rasterized = processing.run("gdal:rasterize", 
                    {
                        'INPUT': groundwater_20_buffer_difference,
                        'FIELD':'',
                        'BURN':1,
                        'UNITS':1,
                        'WIDTH': raster_cell,
                        'HEIGHT': raster_cell,
                        'EXTENT':groundwater_20_buffer.extent(),
                        'NODATA':-9999,
                        'OPTIONS':'',
                        'DATA_TYPE':5,
                        'INIT':None,
                        'INVERT':False,
                        'EXTRA':'',
                        'OUTPUT': 'TEMPORARY_OUTPUT'
                    }, context=context, feedback=feedback)['OUTPUT']
                site_requirements_raster_list.append(groundwater_20_rasterized)
                feedback.setProgressText('Rasterizing Groundwater...')
        
        # combine all the raster only when the pixel value = 1 (that is, suitable)
        suitable_sites = processing.run("native:rasterbooleanand", 
            {
                'INPUT': site_requirements_raster_list,
                'REF_LAYER': site_requirements_raster_list[0],
                'NODATA_AS_FALSE':True,
                'NO_DATA':-9999,
                'DATA_TYPE':5,
                'OUTPUT': 'TEMPORARY_OUTPUT'
            }, context=context, feedback=feedback)['OUTPUT']
        
        # to speed up other queries, convert pixel = 0 in NO DATA
        suitable_sites_no_data = processing.run("gdal:translate", 
            {
                'INPUT': suitable_sites,
                'TARGET_CRS':None,
                'NODATA':0,
                'COPY_SUBDATASETS':False,
                'OPTIONS':'',
                'EXTRA':'',
                'DATA_TYPE':0,
                'OUTPUT':'TEMPORARY_OUTPUT'
            }, context=context, feedback=feedback)['OUTPUT']
        
        # create point for all the pixels filtered (pixel = 1)
        suitable_site_points = processing.run("native:pixelstopoints", 
            {
                'INPUT_RASTER': suitable_sites_no_data,
                'RASTER_BAND':1,
                'FIELD_NAME':'SUITABLE',
                'OUTPUT':'TEMPORARY_OUTPUT'
            }, context=context, feedback=feedback)['OUTPUT']
        feedback.setProgressText('Creating Suitable Points...')

        points_joined_by_location = processing.run("qgis:joinattributesbylocation", 
            {
                'INPUT': suitable_site_points,
                'JOIN': wadi_subbasin_copy,
                'PREDICATE':[0],
                'JOIN_FIELDS':[],
                'METHOD':1,
                'DISCARD_NONMATCHING':False,
                'PREFIX':'',
                'OUTPUT':'TEMPORARY_OUTPUT'
            }, context=context, feedback=feedback)['OUTPUT']
        

        # create new field for the sum of all the zonalstatistics
        zonalsumfield = QgsFields()
        zonalsumfield.append(
            QgsField('ZonalSum', QVariant.Double)
        )
        # get the dataProvider and add the field
        points_joined_by_location_pr = points_joined_by_location.dataProvider()
        points_joined_by_location_pr.addAttributes(zonalsumfield)
        points_joined_by_location.updateFields()

        zonalsumfield_id = points_joined_by_location.fields().lookupField('ZonalSum')

        # create a request with only the desired fields
        request = QgsFeatureRequest()
        request.setSubsetOfAttributes(zonal_statistic_field_names, points_joined_by_location.fields())

        for f in points_joined_by_location.fields():
            feedback.pushDebugInfo(str(f.name()))

        for n, i in enumerate(points_joined_by_location.getFeatures(request)):

            # total sum 
            tsum = 0

            # iterate on the sum for each feature
            for f in zonal_statistic_field_names:
                tsum+=i["{}".format(f)]

            # att the sum of each feature to the field and round it
            attr_value = {zonalsumfield_id:round(tsum, 2)}
            points_joined_by_location_pr.changeAttributeValues({i.id():attr_value})
        

        site_suitability_list = []        

        # locatition of water size 21
        if location_water_site_21:
            location_water_site_21_clipped = processing.run("native:extractbylocation", 
                {
                    'INPUT': location_water_site_21.sourceName(),
                    'PREDICATE': [0],
                    'INTERSECT': wadi_subbasin.sourceName(),
                    'OUTPUT': 'TEMPORARY_OUTPUT'
                }, context=context, feedback=feedback)
            # save the clipped layer into the folder
            QgsVectorFileWriter.writeAsVectorFormat(
                location_water_site_21_clipped['OUTPUT'], 
                os.path.join(
                    output_folder,
                    simulation_folder,
                    'SiteRequirementDataForMultiply',
                    '21_location_water_site_clipped.shp'
                ),
                'utf8', 
                location_water_site_21_clipped['OUTPUT'].crs(), 
                'ESRI Shapefile'
            )
            # make the buffer ONLY if there are features
            if location_water_site_21_clipped['OUTPUT'].featureCount():
                location_water_site_21_buffer = processing.run("native:buffer", 
                    {
                        'INPUT': location_water_site_21_clipped['OUTPUT'],
                        'DISTANCE': location_water_site_distance_21,
                        'SEGMENTS':5,
                        'END_CAP_STYLE':0,
                        'JOIN_STYLE':0,
                        'MITER_LIMIT':2,
                        'DISSOLVE':True,
                        'OUTPUT':'TEMPORARY_OUTPUT'
                    }, context=context, feedback=feedback)['OUTPUT']
                feedback.setProgressText('Creating Location of Water Site Buffer...')

                location_water_site_21_buffer_rasterized = processing.run("gdal:rasterize", 
                    {
                        'INPUT': location_water_site_21_buffer,
                        'FIELD':'',
                        'BURN':1,
                        'UNITS':1,
                        'WIDTH': raster_cell,
                        'HEIGHT': raster_cell,
                        'EXTENT':location_water_site_21_buffer.extent(),
                        'NODATA':-9999,
                        'OPTIONS':'',
                        'DATA_TYPE':5,
                        'INIT':None,
                        'INVERT':False,
                        'EXTRA':'',
                        'OUTPUT': 'TEMPORARY_OUTPUT'
                    }, context=context, feedback=feedback)['OUTPUT']
                feedback.setProgressText('Rasterizing Water Site...')
            
                location_water_site_21_weighted_path = os.path.join(
                    output_folder,
                    simulation_folder,
                    'SiteSuitabilityDataForAdding',
                    '21_location_water_site_weighted'
                )
                location_water_site_21_weighted = processing.run("gdal:rastercalculator", 
                    {
                        'INPUT_A': location_water_site_21_buffer_rasterized,
                        'BAND_A':1, 
                        'FORMULA': f"A*{location_water_site_weight_21}",
                        'NO_DATA':None,
                        'RTYPE':5,
                        'OPTIONS':'',
                        'EXTRA':'',
                        'OUTPUT': location_water_site_21_weighted_path
                    }, context=context, feedback=feedback)
                feedback.setProgressText('Weighting Water Sites...')

                location_water_site_21_weighted_layer = QgsRasterLayer(
                    location_water_site_21_weighted['OUTPUT'],
                    'water_21'
                )
                site_suitability_list.append(location_water_site_21_weighted_layer)


        # locatition of roads 22
        if location_roads_22:
            location_roads_22_clipped = processing.run("native:extractbylocation", 
                {
                    'INPUT': location_roads_22.sourceName(),
                    'PREDICATE': [0],
                    'INTERSECT': wadi_subbasin.sourceName(),
                    'OUTPUT': 'TEMPORARY_OUTPUT'
                }, context=context, feedback=feedback)
            # save the clipped layer into the folder
            QgsVectorFileWriter.writeAsVectorFormat(
                location_roads_22_clipped['OUTPUT'], 
                os.path.join(
                    output_folder,
                    simulation_folder,
                    'SiteRequirementDataForMultiply',
                    '22_location_roads_clipped.shp'
                ),
                'utf8', 
                location_roads_22_clipped['OUTPUT'].crs(), 
                'ESRI Shapefile'
            )
            # make the buffer ONLY if there are features
            if location_roads_22_clipped['OUTPUT'].featureCount():
                location_roads_22_buffer = processing.run("native:buffer", 
                    {
                        'INPUT': location_roads_22_clipped['OUTPUT'],
                        'DISTANCE': location_roads_distance_22,
                        'SEGMENTS':5,
                        'END_CAP_STYLE':0,
                        'JOIN_STYLE':0,
                        'MITER_LIMIT':2,
                        'DISSOLVE':True,
                        'OUTPUT':'TEMPORARY_OUTPUT'
                    }, context=context, feedback=feedback)['OUTPUT']
                feedback.setProgressText('Creating Roads Buffer...')

                location_roads_22_buffer_rasterized = processing.run("gdal:rasterize", 
                    {
                        'INPUT': location_roads_22_buffer,
                        'FIELD':'',
                        'BURN':1,
                        'UNITS':1,
                        'WIDTH': raster_cell,
                        'HEIGHT': raster_cell,
                        'EXTENT':location_roads_22_buffer,
                        'NODATA':-9999,
                        'OPTIONS':'',
                        'DATA_TYPE':5,
                        'INIT':None,
                        'INVERT':False,
                        'EXTRA':'',
                        'OUTPUT': 'TEMPORARY_OUTPUT'
                    }, context=context, feedback=feedback)['OUTPUT']
                feedback.setProgressText('Rasterizing Roads...')
            
                location_roads_22_weighted_path = os.path.join(
                    output_folder,
                    simulation_folder,
                    'SiteSuitabilityDataForAdding',
                    '22_location_roads_weighted'
                )
                location_roads_22_weighted = processing.run("gdal:rastercalculator", 
                    {
                        'INPUT_A': location_roads_22_buffer_rasterized,
                        'BAND_A':1, 
                        'FORMULA': f"A*{location_roads_weight_22}",
                        'NO_DATA':None,
                        'RTYPE':5,
                        'OPTIONS':'',
                        'EXTRA':'',
                        'OUTPUT':location_roads_22_weighted_path
                    }, context=context, feedback=feedback)
                feedback.setProgressText('Weighting Roads...')

                location_roads_22_weighted_layer = QgsRasterLayer(
                    location_roads_22_weighted['OUTPUT'],
                    'roads_22'
                )
                site_suitability_list.append(location_roads_22_weighted_layer)


        # locatition of routes 23
        if location_routes_23:
            location_routes_23_clipped = processing.run("native:extractbylocation", 
                {
                    'INPUT': location_routes_23.sourceName(),
                    'PREDICATE': [0],
                    'INTERSECT': wadi_subbasin.sourceName(),
                    'OUTPUT': 'TEMPORARY_OUTPUT'
                }, context=context, feedback=feedback)
            # save the clipped layer into the folder
            QgsVectorFileWriter.writeAsVectorFormat(
                location_routes_23_clipped['OUTPUT'], 
                os.path.join(
                    output_folder,
                    simulation_folder,
                    'SiteRequirementDataForMultiply',
                    '23_location_routes_clipped.shp'
                ),
                'utf8', 
                location_routes_23_clipped['OUTPUT'].crs(), 
                'ESRI Shapefile'
            )
            # make the buffer ONLY if there are features
            if location_routes_23_clipped['OUTPUT'].featureCount():
                location_routes_23_buffer = processing.run("native:buffer", 
                    {
                        'INPUT': location_routes_23_clipped['OUTPUT'],
                        'DISTANCE': location_routes_distance_23,
                        'SEGMENTS':5,
                        'END_CAP_STYLE':0,
                        'JOIN_STYLE':0,
                        'MITER_LIMIT':2,
                        'DISSOLVE':True,
                        'OUTPUT':'TEMPORARY_OUTPUT'
                    }, context=context, feedback=feedback)['OUTPUT']
                feedback.setProgressText('Creating Routes Buffer...')

                location_routes_23_buffer_rasterized = processing.run("gdal:rasterize", 
                    {
                        'INPUT': location_routes_23_buffer,
                        'FIELD':'',
                        'BURN':1,
                        'UNITS':1,
                        'WIDTH': raster_cell,
                        'HEIGHT': raster_cell,
                        'EXTENT':location_routes_23_buffer,
                        'NODATA':-9999,
                        'OPTIONS':'',
                        'DATA_TYPE':5,
                        'INIT':None,
                        'INVERT':False,
                        'EXTRA':'',
                        'OUTPUT': 'TEMPORARY_OUTPUT'
                    }, context=context, feedback=feedback)['OUTPUT']
                feedback.setProgressText('Rasterizing Routes...')

                location_routes_23_weighted_path = os.path.join(
                    output_folder,
                    simulation_folder,
                    'SiteSuitabilityDataForAdding',
                    '23_location_routes_weighted'
                )
                location_routes_23_weighted = processing.run("gdal:rastercalculator", 
                    {
                        'INPUT_A': location_routes_23_buffer_rasterized,
                        'BAND_A':1, 
                        'FORMULA': f"A*{location_routes_weight_23}",
                        'NO_DATA':None,
                        'RTYPE':5,
                        'OPTIONS':'',
                        'EXTRA':'',
                        'OUTPUT': location_routes_23_weighted_path
                    }, context=context, feedback=feedback)
                feedback.setProgressText('Weighting Routes...')

                location_routes_23_weighted_layer = QgsRasterLayer(
                    location_routes_23_weighted['OUTPUT'],
                    'routes_23'
                )
                site_suitability_list.append(location_routes_23_weighted_layer)


        # locatition of markets 24
        if location_markets_24:
            location_markets_24_clipped = processing.run("native:extractbylocation", 
                {
                    'INPUT': location_markets_24.sourceName(),
                    'PREDICATE': [0],
                    'INTERSECT': wadi_subbasin.sourceName(),
                    'OUTPUT': 'TEMPORARY_OUTPUT'
                }, context=context, feedback=feedback)
            # save the clipped layer into the folder
            QgsVectorFileWriter.writeAsVectorFormat(
                location_markets_24_clipped['OUTPUT'], 
                os.path.join(
                    output_folder,
                    simulation_folder,
                    'SiteRequirementDataForMultiply',
                    '24_location_markets_clipped.shp'
                ),
                'utf8', 
                location_markets_24_clipped['OUTPUT'].crs(), 
                'ESRI Shapefile'
            )
            # make the buffer ONLY if there are features
            if location_markets_24_clipped['OUTPUT'].featureCount():
                location_markets_24_buffer = processing.run("native:buffer", 
                    {
                        'INPUT': location_markets_24_clipped['OUTPUT'],
                        'DISTANCE': location_markets_distance_24,
                        'SEGMENTS':5,
                        'END_CAP_STYLE':0,
                        'JOIN_STYLE':0,
                        'MITER_LIMIT':2,
                        'DISSOLVE':True,
                        'OUTPUT':'TEMPORARY_OUTPUT'
                    }, context=context, feedback=feedback)['OUTPUT']
                feedback.setProgressText('Creating Markets Buffer...')

                location_markets_24_buffer_rasterized = processing.run("gdal:rasterize", 
                    {
                        'INPUT': location_markets_24_buffer,
                        'FIELD':'',
                        'BURN':1,
                        'UNITS':1,
                        'WIDTH': raster_cell,
                        'HEIGHT': raster_cell,
                        'EXTENT':location_markets_24_buffer.extent(),
                        'NODATA':-9999,
                        'OPTIONS':'',
                        'DATA_TYPE':5,
                        'INIT':None,
                        'INVERT':False,
                        'EXTRA':'',
                        'OUTPUT': 'TEMPORARY_OUTPUT'
                    }, context=context, feedback=feedback)['OUTPUT']
                feedback.setProgressText('Rasterizing Markets...')
            
                location_markets_24_weighted_path = os.path.join(
                    output_folder,
                    simulation_folder,
                    'SiteSuitabilityDataForAdding',
                    '24_location_markets_weighted'
                )
                location_markets_24_weighted = processing.run("gdal:rastercalculator", 
                    {
                        'INPUT_A': location_markets_24_buffer_rasterized,
                        'BAND_A':1, 
                        'FORMULA': f"A*{location_markets_weight_24}",
                        'NO_DATA':None,
                        'RTYPE':5,
                        'OPTIONS':'',
                        'EXTRA':'',
                        'OUTPUT': location_markets_24_weighted_path
                    }, context=context, feedback=feedback)
                feedback.setProgressText('Weighting Markets...')

                location_markets_24_weighted_layer = QgsRasterLayer(
                    location_markets_24_weighted['OUTPUT'],
                    'market_24'
                )
                site_suitability_list.append(location_markets_24_weighted_layer)


        # locatition of districts 25
        if location_districts_25:
            location_districts_25_clipped = processing.run("native:extractbylocation", 
                {
                    'INPUT': location_districts_25.sourceName(),
                    'PREDICATE': [0],
                    'INTERSECT': wadi_subbasin.sourceName(),
                    'OUTPUT': 'TEMPORARY_OUTPUT'
                }, context=context, feedback=feedback)
            # save the clipped layer into the folder
            QgsVectorFileWriter.writeAsVectorFormat(
                location_districts_25_clipped['OUTPUT'], 
                os.path.join(
                    output_folder,
                    simulation_folder,
                    'SiteRequirementDataForMultiply',
                    '25_location_districts_clipped.shp'
                ),
                'utf8', 
                location_districts_25_clipped['OUTPUT'].crs(), 
                'ESRI Shapefile'
            )
            # make the buffer ONLY if there are features
            if location_districts_25_clipped['OUTPUT'].featureCount():
                location_districts_25_buffer = processing.run("native:buffer", 
                    {
                        'INPUT': location_districts_25_clipped['OUTPUT'],
                        'DISTANCE': location_districts_distance_25,
                        'SEGMENTS':5,
                        'END_CAP_STYLE':0,
                        'JOIN_STYLE':0,
                        'MITER_LIMIT':2,
                        'DISSOLVE':True,
                        'OUTPUT':'TEMPORARY_OUTPUT'
                    }, context=context, feedback=feedback)['OUTPUT']
                feedback.setProgressText('Creating Districts Buffer...')

                location_districts_25_rasterized = processing.run("gdal:rasterize", 
                    {
                        'INPUT': location_districts_25_buffer,
                        'FIELD':'',
                        'BURN':1,
                        'UNITS':1,
                        'WIDTH': raster_cell,
                        'HEIGHT': raster_cell,
                        'EXTENT':location_districts_25_buffer.extent(),
                        'NODATA':-9999,
                        'OPTIONS':'',
                        'DATA_TYPE':5,
                        'INIT':None,
                        'INVERT':False,
                        'EXTRA':'',
                        'OUTPUT': 'TEMPORARY_OUTPUT'
                    }, context=context, feedback=feedback)['OUTPUT']
                feedback.setProgressText('Rasterizing Districts...')
            
                location_districts_25_rasterized_path = os.path.join(
                    output_folder,
                    simulation_folder,
                    'SiteSuitabilityDataForAdding',
                    '25_location_districts_buffer_rasterized'
                )
                location_districts_25_weighted = processing.run("gdal:rastercalculator", 
                    {
                        'INPUT_A': location_districts_25_rasterized,
                        'BAND_A':1, 
                        'FORMULA': f"A*{location_districts_weight_25}",
                        'NO_DATA':None,
                        'RTYPE':5,
                        'OPTIONS':'',
                        'EXTRA':'',
                        'OUTPUT': location_districts_25_rasterized_path
                    }, context=context, feedback=feedback)
                feedback.setProgressText('Weighting Districts...')

                location_districts_25_weighted_layer = QgsRasterLayer(
                    location_districts_25_weighted['OUTPUT'],
                    'distr_25'
                )
                site_suitability_list.append(location_districts_25_weighted_layer)

        # locatition of settlements 26
        if settlements_26:
            settlements_26_clipped = processing.run("native:extractbylocation", 
                {
                    'INPUT': settlements_26.sourceName(),
                    'PREDICATE': [0],
                    'INTERSECT': wadi_subbasin.sourceName(),
                    'OUTPUT': 'TEMPORARY_OUTPUT'
                }, context=context, feedback=feedback)
            # save the clipped layer into the folder
            QgsVectorFileWriter.writeAsVectorFormat(
                settlements_26_clipped['OUTPUT'], 
                os.path.join(
                    output_folder,
                    simulation_folder,
                    'SiteRequirementDataForMultiply',
                    '26_settlements_clipped.shp'
                ),
                'utf8', 
                settlements_26_clipped['OUTPUT'].crs(), 
                'ESRI Shapefile'
            )
            # make the buffer ONLY if there are features
            if settlements_26_clipped['OUTPUT'].featureCount():
                settlements_26_buffer = processing.run("native:buffer", 
                    {
                        'INPUT': settlements_26_clipped['OUTPUT'],
                        'DISTANCE': settlements_distance_26,
                        'SEGMENTS':5,
                        'END_CAP_STYLE':0,
                        'JOIN_STYLE':0,
                        'MITER_LIMIT':2,
                        'DISSOLVE':True,
                        'OUTPUT':'TEMPORARY_OUTPUT'
                    }, context=context, feedback=feedback)['OUTPUT']
                feedback.setProgressText('Creating Settlements Buffer...')

                settlements_26_buffer_rasterized = processing.run("gdal:rasterize", 
                    {
                        'INPUT': settlements_26_buffer,
                        'FIELD':'',
                        'BURN':1,
                        'UNITS':1,
                        'WIDTH': raster_cell,
                        'HEIGHT': raster_cell,
                        'EXTENT':settlements_26_buffer.extent(),
                        'NODATA':-9999,
                        'OPTIONS':'',
                        'DATA_TYPE':5,
                        'INIT':None,
                        'INVERT':False,
                        'EXTRA':'',
                        'OUTPUT': 'TEMPORARY_OUTPUT'
                    }, context=context, feedback=feedback)['OUTPUT']
                feedback.setProgressText('Rasterizing Settlements...')

                settlements_26_weighted_path = os.path.join(
                    output_folder,
                    simulation_folder,
                    'SiteSuitabilityDataForAdding',
                    '26_settlements_weighted'
                )
                settlements_26_weighted = processing.run("gdal:rastercalculator", 
                    {
                        'INPUT_A': settlements_26_buffer_rasterized,
                        'BAND_A':1, 
                        'FORMULA': f"A*{settlements_weight_26}",
                        'NO_DATA':None,
                        'RTYPE':5,
                        'OPTIONS':'',
                        'EXTRA':'',
                        'OUTPUT': settlements_26_weighted_path
                    }, context=context, feedback=feedback)
                feedback.setProgressText('Weighting Settlements...')
                
                settlements_26_weighted_layer = QgsRasterLayer(
                    settlements_26_weighted['OUTPUT'],
                    'sett_26'
                )
                site_suitability_list.append(settlements_26_weighted_layer)


        # sample the suitable_site_points with all the rasters of the list
        # create transformation context

        # create dataProvider
        points_joined_by_location_pr = points_joined_by_location.dataProvider()
        point_fields = QgsFields()

        for idx, rlayer in enumerate(site_suitability_list):

            # add as many fields as the rasters in the the list with its name
            point_fields.append(
                QgsField(
                    rlayer.name(),
                    QVariant.Double
                )
            )

            feedback.setProgressText('Sampling {} of {} raster layers...'.format(idx, len(site_suitability_list)))

            points_joined_by_location_pr.addAttributes(point_fields)
            points_joined_by_location.updateFields()


            for n, i in enumerate(points_joined_by_location_pr.getFeatures()):

                # get feature and field id
                feat_id = i.id()
                field_id = points_joined_by_location.fields().lookupField(rlayer.name())

                # get point geometry and raster data provider
                geom = i.geometry()    
                rlayer_data_provider = rlayer.dataProvider()

                # i[rlayer.name()] 

                # numeric value of sampled point
                sampled_value = rlayer_data_provider.identify(
                    geom.asPoint(), 
                    QgsRaster.IdentifyFormatValue
                ).results()[1]

                attr_value = {field_id:sampled_value}
                points_joined_by_location_pr.changeAttributeValues({feat_id:attr_value})
        

        # write the suitability for each points as the sum of all the calculated fields
        # add the field
        point_fields = QgsFields()
        point_fields.append(
            QgsField('SiteSuit', QVariant.Double)
        )
        points_joined_by_location_pr.addAttributes(point_fields)
        points_joined_by_location.updateFields()
        sitesuitefield_id = points_joined_by_location.fields().lookupField('SiteSuit')

        for n, i in enumerate(points_joined_by_location.getFeatures()):

            # total sum 
            tsum = 0

            for f in site_suitability_list:

                if i[f.name()] != NULL:
                    tsum+=i[f.name()]

            attr_value = {sitesuitefield_id:tsum}
            points_joined_by_location_pr.changeAttributeValues({i.id():attr_value})

        # write the final and total suitability as the sum of zonal statistics and site suitability
        # add the field
        point_fields = QgsFields()
        point_fields.append(
            QgsField('TotalSuit', QVariant.Double)
        )
        points_joined_by_location_pr.addAttributes(point_fields)
        points_joined_by_location.updateFields()
        totalsuite_field_id = points_joined_by_location.fields().lookupField('TotalSuit')

        for n, i in enumerate(points_joined_by_location.getFeatures()):

            tsumt = i['ZonalSum'] + i['SiteSuit']
            tsum = round(tsumt, 2)

            attr_value = {totalsuite_field_id:tsum}
            points_joined_by_location_pr.changeAttributeValues({i.id():attr_value})

        # set the style from file
        sty = os.path.join(pluginPath, 'style.qml')
        feedback.pushDebugInfo(str(sty))
        processing.run("qgis:setstyleforvectorlayer",
            {
                'INPUT': points_joined_by_location,
                'STYLE':sty
            }, context=context, feedback=feedback)

        context.temporaryLayerStore().addMapLayer(points_joined_by_location)
        context.addLayerToLoadOnCompletion(points_joined_by_location.source(), context.LayerDetails(
            name='WADI_simulatio_{}.shp'.format(simulation_id),
            project=context.project()
        ))

                # write into the output folder the maerialized copy of wadi_subbasin_copy layer in the folder
        QgsVectorFileWriter.writeAsVectorFormat(
            points_joined_by_location, 
            os.path.join(
                output_folder,
                simulation_folder, 
                'WadiName_{}_SiteSuitability.shp'.format(simulation_id)
            ), 
            'utf8', 
            wadi_subbasin_copy.crs(), 
            'ESRI Shapefile'
        )


        # TODO 27, 28 and 29 are the really necessary? I don't get what these layers are used for
        
        results = {
            # self.SITE_REQUIREMENTS_OUTPUT: site_requirements_output_raster,
            self.OUTPUT_FOLDER: output_folder
        }


        return results
